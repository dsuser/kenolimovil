import React, { Component } from 'react';
// import { View, AsyncStorage, Button, StatusBar} from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import {Alert, Text, View, Button, StatusBar, TouchableOpacity, AsyncStorage, ActivityIndicator, ScrollView, KeyboardAvoidingView,FlatList, Platform} from 'react-native';
import Icon from "react-native-vector-icons/MaterialIcons";
import * as SQLite from 'expo-sqlite';
import styles from '../styles/styles';

const db = SQLite.openDatabase('answersData.db');

export default class OtherScreen extends Component {
	static navigationOptions = {
    	title: 'Datos del Miembro',
  	};

  	constructor(props){
  		super(props);
  		this.state = {
  		data: "",
  		question: [],
        loading: true,
        resultado: [],
        form_id: "",
        order:0,
        not_saved: true,
  		}
   }

  	componentDidMount(){
  		this._retrieveData();
  	}

  	_retrieveData = async () => {
      // let string_ans = this.props.navigation.getParam('answers');
      // let previous_answers = JSON.parse(string_ans);
      this._getStoredQuestions(this.props.navigation.getParam('form_id'));
    }

    _getStoredQuestions = (form_id) => {
       db.transaction(tx => {
       tx.executeSql('SELECT question_array FROM question_table WHERE form_id =? ', [form_id], 
       (tx, results) => {
         let questions = JSON.parse(results.rows._array[0]["question_array"]);
         let data_member = [];
         for(const prop in this.props.navigation.getParam('member')){
         	if(questions.data.find(data => data['orden'] == prop) != undefined){
         		if(questions.data.find(data => data['orden'] == prop)['type_result'] != 9 && (questions.data.find(data => data['orden'] == prop)['title_question']).trim() != "¿Agregar un miembro más?" ){
         			data_member.push(questions.data.find(data => data['orden'] == prop))
         		}
         	}
         }
        this.setState({data_member: data_member});
       });
      });
    }


	render() {
		if(this.state.data_member == ""){
			return(
				<View style={[styles.loadingContainer, styles.centerContainer]}> 
        			<ActivityIndicator size="large" color="#0000ff" />
        			<Text>Cargando Preguntas...</Text>
        		</View>
        	);
    	}else{
    		console.disableYellowBox = true;
			return(
				<View>
        			 <FlatList
                            key = {Math.random()}
                            data={this.state.data_member}
                            numColumns={1}
                            scrollEnabled={true}
                            renderItem={
                                ({item, index}) =>{
                                    if(item.title_question){
                                        return(
                                            <View key = {Math.random()}>                                                                   
                                                <TouchableOpacity style={[styles.childRowQ, styles.buttonQ]} onPress={()=>this._editQuestion(item.orden, this.props.previous_answers)}>
                                                <Icon name={'edit'} size={24} color={'rgba(0,102,204,0.5)'} />
                                                <Text>{"   "}</Text>
                                                <Text>{item.title_question}</Text>
                                                </TouchableOpacity>
                                                <View style={styles.childHr}/>
                                            </View>
                                        );
                                    }else{
                                        return(
                                            <View key = {Math.random()}>
                                                <TouchableOpacity  style={[styles.childRowQ, styles.buttonQ]} onPress={()=>this._editMember(item)}>
                                                <Icon  name={'edit'} size={24} color={'rgba(0,102,204,0.5)'} />
                                                <Text >{"   "}</Text>
                                                <Text >{item['41']}</Text>
                                                <Text >{item.form_id}</Text>
                                                </TouchableOpacity>
                                                <View  style={styles.childHr}/>
                                            </View>
                                        );
                                    }
                                } 
                            }
                            keyExtractor={(item) => {Math.random()}}
                            />
      			</View>
			);
		}
	}

	_signOutAsync = async () => {
		await AsyncStorage.clear();
    	this.props.navigation.navigate('Auth');
  	};
}
