import React, { Component } from 'react';
import { View, AsyncStorage, Button, StatusBar, FlatList, Text, TouchableOpacity, Alert} from 'react-native';
import * as SQLite from 'expo-sqlite';
import styles from '../styles/styles';
//Url del Rest
import urlBase from '../enviroment/url';
import Icon from "react-native-vector-icons/MaterialIcons";
const now = new Date();
const db = SQLite.openDatabase('answersData.db');


export default class RegisterScreen extends Component {
	static navigationOptions = {
    	title: 'Cuestionarios Guardados',
  	};

  	constructor(props) {
    super(props);
    this.state = {
       FlatListItems: [],
       url_gateway: urlBase+'log/gateway/input',
       url: urlBase+'admin/movil/json',
       disabled: false, //estado de botones
    };

   
  }
  componentDidMount(){
  	 db.transaction(tx => {
      tx.executeSql('SELECT * FROM answer_table ORDER BY code_form', [], 
      (tx, results) => {
      	console.log(results);
        var temp = [];
        for (let i = 0; i < results.rows.length; ++i) {
          temp.push(results.rows.item(i));
        }
        this.setState({FlatListItems: temp});
      });
    });
  }

   ListViewItemSeparator = () => {
    return (
      <View style={{ height: 1, marginBottom: 3, width: '100%', backgroundColor: 'rgba(33, 47,47,0.01)', elevation:100 }} />
    );
  };

  updateList = id => {
  	const filteredData = this.state.FlatListItems.filter(item => item.id !== id);
  	this.setState({ FlatListItems: filteredData });
  }

  _DeleteData(item_id){
  	 db.transaction(txn => {
      		txn.executeSql(
        		"DELETE FROM answer_table WHERE id = ? ",[item_id],     
	        (tx, res) => {
	                  if (res.rowsAffected > 0) {
	                  Alert.alert(
	                    'Informacion!!','Acciones realizadas exitosamente',
	                    [
	                      {
	                        text: 'Ok',
	                        onPress: () => {this.updateList(item_id)}
	                      },
	                    ],
	                    { cancelable: false }
	                  );
	                } else {
	                  Alert.alert('Algo ha salido mal');
	                }
	        });
    	});

  }

  _SendData = (item_id, answers) => {
  	
  	console.log(answers);
  	this.setState(prevState =>({ 
      disabled: !prevState.disabled 
    }));
    var formData = new FormData();
    formData.append("answers", answers);
    fetch(this.state.url_gateway, {
      method: 'POST',
      headers: { 'Accept': 'application/json','Content-Type': 'multipart/form-data',}, 
      body: formData,
    })
    .then(response => response.json())
    .then(respuesta => {
    	console.log(respuesta);
    	if(respuesta.ok){
    		Alert.alert(
                      'Informacion!!',''+respuesta.msg,
                      [
                        {
                          text: 'Ok',
                          onPress: () => {this._DeleteData(item_id);this.setState({disabled:false})}
                        },
                      ],
                      { cancelable: false }
            );
    	}
    	else{
    		Alert.alert('Informacion!!',''+respuesta.msg,
                      [
                        {
                          text: 'Ok',
                          onPress: () => {this._DeleteData(item_id);this.setState({disabled:false})}
                        },
                      ],
                      { cancelable: false }); 
    	}
      })
      .catch((error) => {
        console.log(error)
      });
  }

  _editData = (item_id, answer_array) => {
      this.props.navigation.navigate('ListQuestion',{id: item_id, answers: answer_array});
  }
  
	render() {
		return (
			    <View>
			        <FlatList
			          data={this.state.FlatListItems}
			          ItemSeparatorComponent={this.ListViewItemSeparator}
			          keyExtractor={(item, index) => index.toString()}
			          renderItem={({ item }) => {
			          	let codigo_form = JSON.parse(item.answer_array).form;
			          	if(codigo_form == "C30"){
			          		return(
				          		<View key={item.id} style={styles.itemList}>

				          			<View style={styles.infoLeft}>
				          				<View style={styles.actionsButtons}>
						            		<Text style={styles.boldText}>Cuestionario: </Text>
						            		<Text style={styles.normalText}>{JSON.parse(item.answer_array).form}</Text>
						            	</View>

							            <View style={styles.actionsButtons}>
							            	<Text style={styles.boldText}>ID: </Text>
							            	<Text style={styles.normalText}>{JSON.parse(item.answer_array).uuidMD5}</Text>
							            </View>

							             <View style={styles.actionsButtons}>
							            	<Text style={styles.boldText}>Participante: </Text>
							            	<Text style={styles.normalText}>{JSON.parse(item.answer_array)[0.1]}</Text>
							            </View>
				          			</View>
						            

				          			<View style={styles.infoRight}>
						              	<View style={styles.actionsButtons}>
							              <TouchableOpacity style={this.state.disabled ?  [styles.buttonDisabled] : [styles.buttonNext, styles.sendButtons]} onPress={()=>this._SendData(item.id, item.answer_array)} disabled={this.state.disabled}>
							                        <Text style={styles.buttonText} ><Icon name={"send"} size={30} color={'rgb(255, 255,255)'}/> Enviar</Text>
							              </TouchableOpacity>
							              <TouchableOpacity style={this.state.disabled ?  [styles.buttonDisabled] : [styles.buttonNext, styles.editButtons]} onPress={()=>this._editData(item.id, item.answer_array)} disabled={this.state.disabled}>
							                        <Text style={styles.buttonText} ><Icon name={"edit"} size={30} color={'rgb(255, 255,255)'}/> Editar</Text>
							              </TouchableOpacity>
							               <TouchableOpacity style={this.state.disabled ?  [styles.buttonDisabled] : [styles.buttonNext, styles.deleteButtons]} onPress={()=>this._DeleteData(item.id)} disabled={this.state.disabled}>
							                        <Text style={styles.buttonText} ><Icon name={"clear"} size={30} color={'rgb(255, 255,255)'}/> Eliminar</Text>
							              </TouchableOpacity>
						             	</View>
					             	</View>


				            	</View>
			            	)
			          	}else if (codigo_form == "C31") {
			          		return(
				          		<View key={item.id} style={styles.itemList}>

				          			<View style={styles.infoLeft}>
				          				<View style={styles.actionsButtons}>
						            		<Text style={styles.boldText}>Cuestionario: </Text>
						            		<Text style={styles.normalText}>{JSON.parse(item.answer_array).form}</Text>
						            	</View>

							            <View style={styles.actionsButtons}>
							            	<Text style={styles.boldText}>ID: </Text>
							            	<Text style={styles.normalText}>{JSON.parse(item.answer_array).uuidMD5}</Text>
							            </View>

							             <View style={styles.actionsButtons}>
							            	<Text style={styles.boldText}>Participante: </Text>
							            	<Text style={styles.normalText}>{JSON.parse(item.answer_array)[0.2]}</Text>
							            </View>
				          			</View>
						            

				          			<View style={styles.infoRight}>
						              	<View style={styles.actionsButtons}>
							              <TouchableOpacity style={this.state.disabled ?  [styles.buttonDisabled] : [styles.buttonNext, styles.sendButtons]} onPress={()=>this._SendData(item.id, item.answer_array)} disabled={this.state.disabled}>
							                        <Text style={styles.buttonText} ><Icon name={"send"} size={30} color={'rgb(255, 255,255)'}/> Enviar</Text>
							              </TouchableOpacity>
							              <TouchableOpacity style={this.state.disabled ?  [styles.buttonDisabled] : [styles.buttonNext, styles.editButtons]} onPress={()=>this._editData(item.id, item.answer_array)} disabled={this.state.disabled}>
							                        <Text style={styles.buttonText} ><Icon name={"edit"} size={30} color={'rgb(255, 255,255)'}/> Editar</Text>
							              </TouchableOpacity>
							               <TouchableOpacity style={this.state.disabled ?  [styles.buttonDisabled] : [styles.buttonNext, styles.deleteButtons]} onPress={()=>this._DeleteData(item.id)} disabled={this.state.disabled}>
							                        <Text style={styles.buttonText} ><Icon name={"clear"} size={30} color={'rgb(255, 255,255)'}/> Eliminar</Text>
							              </TouchableOpacity>
						             	</View>
					             	</View>


				            	</View>
			            	)
			          	}
			          	else if(codigo_form == "C32"){
			          		return(
				          		<View key={item.id} style={styles.itemList}>
				          			<View style={styles.infoLeft}>
							            <View style={styles.actionsButtons}>
							            	<Text style={styles.boldText}>Cuestionario: </Text>
							            	<Text style={styles.normalText}>{JSON.parse(item.answer_array).form}</Text>
							            </View>

							            <View style={styles.actionsButtons}>
							            	<Text style={styles.boldText}>ID: </Text>
							            	<Text style={styles.normalText}>{JSON.parse(item.answer_array).uuidMD5}</Text>
							            </View>

							             <View style={styles.actionsButtons}>
							            	<Text style={styles.boldText}>Participante: </Text>
							            	<Text style={styles.normalText}>{JSON.parse(item.answer_array)[0.1]}</Text>
							            </View>
							        </View>
					             	
					             	<View style={styles.infoRight}>
						              	<View style={styles.actionsButtons}>
							              <TouchableOpacity style={this.state.disabled ?  [styles.buttonDisabled] : [styles.buttonNext, styles.sendButtons]} onPress={()=>this._SendData(item.id, item.answer_array)} disabled={this.state.disabled}>
							                        <Text style={styles.buttonText} ><Icon name={"send"} size={30} color={'rgb(255, 255,255)'}/> Enviar</Text>
							              </TouchableOpacity>
							              <TouchableOpacity style={this.state.disabled ?  [styles.buttonDisabled] : [styles.buttonNext, styles.editButtons]} onPress={()=>this._editData(item.id, item.answer_array)} disabled={this.state.disabled}>
							                        <Text style={styles.buttonText} ><Icon name={"edit"} size={30} color={'rgb(255, 255,255)'}/> Editar</Text>
							              </TouchableOpacity>
							               <TouchableOpacity style={this.state.disabled ?  [styles.buttonDisabled] : [styles.buttonNext, styles.deleteButtons]} onPress={()=>this._DeleteData(item.id)} disabled={this.state.disabled}>
							                        <Text style={styles.buttonText} ><Icon name={"clear"} size={30} color={'rgb(255, 255,255)'}/> Eliminar</Text>
							              </TouchableOpacity>
						             	</View>
						            </View>
				            	</View>
			            	)
			          	}
			          	if(codigo_form == "C40" || codigo_form == "C41" || codigo_form == "C42"){
			          		return(
				          		<View key={item.id} style={styles.itemList}>
				          			<View style={styles.infoLeft}>
							            <View style={styles.actionsButtons}>
							            	<Text style={styles.boldText}>Cuestionario: </Text>
							            	<Text style={styles.normalText}>{JSON.parse(item.answer_array).form}</Text>
							            </View>

							            <View style={styles.actionsButtons}>
							            	<Text style={styles.boldText}>ID: </Text>
							            	<Text style={styles.normalText}>{JSON.parse(item.answer_array).uuidMD5}</Text>
							            </View>

							             <View style={styles.actionsButtons}>
							            	<Text style={styles.boldText}>Participante: </Text>
							            	<Text style={styles.normalText}>{JSON.parse(item.answer_array).name_participant}</Text>
							            </View>
							         </View>
					             	<View style={styles.infoRight}>
						              	<View style={styles.actionsButtons}>
							              <TouchableOpacity style={this.state.disabled ?  [styles.buttonDisabled] : [styles.buttonNext, styles.sendButtons]} onPress={()=>this._SendData(item.id, item.answer_array)} disabled={this.state.disabled}>
							                        <Text style={styles.buttonText} ><Icon name={"send"} size={30} color={'rgb(255, 255,255)'}/> Enviar</Text>
							              </TouchableOpacity>
							              <TouchableOpacity style={this.state.disabled ?  [styles.buttonDisabled] : [styles.buttonNext, styles.editButtons]} onPress={()=>this._editData(item.id, item.answer_array)} disabled={this.state.disabled}>
							                        <Text style={styles.buttonText} ><Icon name={"edit"} size={30} color={'rgb(255, 255,255)'}/> Editar</Text>
							              </TouchableOpacity>
							               <TouchableOpacity style={this.state.disabled ?  [styles.buttonDisabled] : [styles.buttonNext, styles.deleteButtons]} onPress={()=>this._DeleteData(item.id)} disabled={this.state.disabled}>
							                        <Text style={styles.buttonText} ><Icon name={"clear"} size={30} color={'rgb(255, 255,255)'}/> Eliminar</Text>
							              </TouchableOpacity>
						             	</View>
						             </View>
				            	</View>
			            	)
			          	}
			          }
			      	}
			        />
			    </View>
    	);
	}

	_signOutAsync = async () => {
		await AsyncStorage.clear();
    	this.props.navigation.navigate('Auth');
  	};
}
