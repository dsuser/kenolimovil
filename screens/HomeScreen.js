import React, { Component } from 'react';
import { Text, ActivityIndicator, View, AsyncStorage, Button, ScrollView, TouchableHighlight, TouchableOpacity} from 'react-native';
import { Card, ListItem, Icon } from 'react-native-elements'
import * as SQLite from 'expo-sqlite';
import urlBase     from '../enviroment/url';
import styles      from '../styles/styles';
const now = new Date();
const db = SQLite.openDatabase('answersData.db');
const no_participant = "Aun no hay participantes disponibles para este monitoreo";
export default class HomeScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      user: '',
      desconectar: undefined,
    }
  
    
  }
  static navigationOptions = {
    title: 'Fundacion Kenoli',
  };

  componentDidMount(){
    this._retrieveData();
    this._createAnswerTable();
    this._createParticipantTable();
    this._createQuestionTable();
    this._createKenoliTable();
    this._createConectionTable();
    this._setButtonState();
  }

  _createAnswerTable(){
    db.transaction(txn => {
      txn.executeSql(
        "SELECT name from sqlite_master WHERE type = 'table' AND name = 'answer_table'",[],     
        function(tx, res) {
          if(res.rows.length == 0){
            txn.executeSql("DROP TABLE IF EXISTS answer_table", []);
            txn.executeSql("CREATE TABLE IF NOT EXISTS answer_table(id INTEGER PRIMARY KEY AUTOINCREMENT, code_form VARCHAR(5), answer_array TEXT)",[]);
          }
        }   
      );
    });
  }

  _createParticipantTable(){
    db.transaction(txn => {
        txn.executeSql(
          "SELECT name from sqlite_master WHERE type = 'table' AND name = 'participants_table'",[],     
          function(tx, res) {
          if(res.rows.length == 0){
            txn.executeSql("DROP TABLE IF EXISTS participants_table", []);
            txn.executeSql("CREATE TABLE IF NOT EXISTS participants_table(id INTEGER PRIMARY KEY AUTOINCREMENT, participants_array TEXT, form_id INTEGER, community INTEGER)",[]);
            }
          }   
          );
     });
  }

  _createQuestionTable(){
    db.transaction(txn => {
        txn.executeSql(
          "SELECT name from sqlite_master WHERE type = 'table' AND name = 'question_table'",[],     
          function(tx, res) {
          if(res.rows.length == 0){
            txn.executeSql("DROP TABLE IF EXISTS question_table", []);
            txn.executeSql("CREATE TABLE IF NOT EXISTS question_table(id INTEGER PRIMARY KEY AUTOINCREMENT, question_array TEXT, form_id INTEGER)",[]);
            }
          }   
          );
        });
  }

  _createKenoliTable(){
    db.transaction(txn => {
        txn.executeSql(
          "SELECT name from sqlite_master WHERE type = 'table' AND name = 'kenolif_table'",[],     
          function(tx, res) {
          if(res.rows.length == 0){
            txn.executeSql("DROP TABLE IF EXISTS kenolif_table", []);
            txn.executeSql("CREATE TABLE IF NOT EXISTS kenolif_table(id INTEGER PRIMARY KEY AUTOINCREMENT, forms_array TEXT)",[]);
            }
          }   
          );
        });
  }

  _createConectionTable(){
    db.transaction(txn => {
        txn.executeSql(
          "SELECT name from sqlite_master WHERE type = 'table' AND name = 'connection_table'",[],     
          function(tx, res) {
          if(res.rows.length == 0){
            txn.executeSql("DROP TABLE IF EXISTS connection_table", []);
            txn.executeSql("CREATE TABLE IF NOT EXISTS connection_table(id INTEGER PRIMARY KEY AUTOINCREMENT, isOffline TEXT)",[]);
            }
          });
        });
  }

  _retrieveData = async () => {
  try {
    const user = await AsyncStorage.getItem('user');
      if (user !== null) {
       this.setState({user: JSON.parse(user)});
      }
    } catch (error) {
      console.log(error);
    }
  };
  
  render() {
    console.disableYellowBox = true;
    if(this.state.user == ''){
      return(
                <View style={[styles.loadingContainer, styles.centerContainer]}> 
                  <ActivityIndicator size="large" color="#0000ff" />
                  <Text>Cargando...</Text>
                </View>
            );
    }
    else{
      return (
        <ScrollView>
          <View style={{ padding: 10, paddingBottom: 0 }}>
              {/*Boton para deshabilitar las funciones de red*/} 
              <View style={styles.controlButtons}>
                <TouchableOpacity style={(this.state.desconectar) ? [styles.buttonDisabled] : [styles.buttonEnabled] } onPress={()=>this._changeWork()} disabled={(this.state.disabled)}>
                  <Text style={styles.buttonText} >{(this.state.desconectar) ? 'Trabajando sin Internet' : 'Trabajando con Internet' }</Text>
                </TouchableOpacity>
              </View>
             {/*Fin boton para deshabilitar las funciones de red*/}
              <Text style={{ fontSize: 13, color: "rgba(0, 0, 0, 0.5)" }} >
                  {now.toDateString()}
              </Text>
              <Text style={{ fontSize: 32, fontWeight: "bold" }}>Bienvenido {this.state.user[0].local_username}</Text>
          </View>

          <TouchableHighlight onPress={this._showCommunities}>
            <Card containerStyle={[{ borderRadius: 20, elevation: 0, height: 225 }]} title='Ingreso de cuestionarios' image={require('../assets/images/cuestionario.jpg')}>
            <View style={styles.footerCard}>
               
              </View>
            </Card>
          </TouchableHighlight> 

           <TouchableHighlight onPress={this._showRegister}>
            <Card containerStyle={[{ borderRadius: 20, elevation: 0, height: 225 }]} title='Cuestionarios Guardados' image={require('../assets/images/saved.png')}>
            <View style={styles.footerCard}>
               
              </View>
            </Card>
          </TouchableHighlight> 

           <TouchableHighlight style={[{paddingBottom: 15}]} onPress={this._signOutAsync}>
            <Card containerStyle={[{ borderRadius: 20, elevation: 0, height: 225}]} title='Cerrar Sesión' image={require('../assets/images/exit.png')}>
              <View style={styles.footerCard}>
               
              </View>
            </Card>
          </TouchableHighlight> 

          
        </ScrollView>
      );
    }
  }

  _changeWork = () =>{
    this.setState({desconectar: !this.state.desconectar}, () => { this._localStorage(this.state.desconectar) });
  }

  _localStorage = (disconnetc) => {
    if (disconnetc) {
      this._populateParticipants();
      this._populateKenoliTable();
      this._populateQuestionTable(1,this.state.user[0].idUserLog);
      this._populateQuestionTable(2,this.state.user[0].idUserLog);
      this._populateQuestionTable(3,this.state.user[0].idUserLog);
      this._populateQuestionTable(4,this.state.user[0].idUserLog);
      this._populateQuestionTable(5,this.state.user[0].idUserLog);
      this._populateQuestionTable(6,this.state.user[0].idUserLog);
      // this._populateParticipants();
      this._setWorkMethod("TRUE");
    }
    else{
      this._setWorkMethod("FALSE");
    }
    
  }

  _populateKenoliTable(){
    var form_id = 1;
    fetch(urlBase+'admin/movil/forms/json', {
        method: 'POST',
        headers: { 'Accept': 'application/json','Content-Type': 'multipart/form-data',}, 
    })
    .then(response => response.json())
    .then(forms => {
      if(forms != []){
        var vacio = false;
        db.transaction(txn => {
          txn.executeSql(
            "SELECT * from kenolif_table where id = ?",[form_id],     
            function(tx, res) {
              if(res.rows.length == 0){
                vacio = true;
                  if(vacio){
                    txn.executeSql("INSERT INTO kenolif_table (forms_array) VALUES (?)",[JSON.stringify(forms)],     
                    (tx, res) => {
                      (res.rowsAffected > 0) ? console.log("Formularios Guardados") : console.log("Algo Salio Mal");
                    });
                  }
              }
              else{
                txn.executeSql("UPDATE kenolif_table SET forms_array = ? WHERE id = ?",[JSON.stringify(forms),form_id],     
                (tx, res) => {
                  (res.rowsAffected > 0) ? console.log("Formularios Actualizados") : console.log("Algo Salio Mal");
                });
              }
            });
          });
      }
    });
  }

  _populateQuestionTable(id_form, user_id){
      console.log("llenado de preguntas");
      //var user_id = this.state.user[0].idUserLog;
      var formData = new FormData();

      formData.append("form_id", id_form);
      formData.append("user_id", user_id);
      fetch(urlBase+'admin/movil/json', {
        method: 'POST',
        headers: { 'Accept': 'application/json','Content-Type': 'multipart/form-data',}, 
        body: formData,
      })
      .then(response => response.json())
      .then(questions => {
        if(questions.data.length > 0){
          if(questions != []){
          var vacio = false;
          db.transaction(txn => {
            txn.executeSql(
            "SELECT * from question_table WHERE form_id = ? ",[id_form],     
            function(tx, res) {
              if(res.rows.length == 0){
                vacio = true;
                  if(vacio){
                    txn.executeSql("INSERT INTO question_table (question_array, form_id) VALUES (?,?)",[JSON.stringify(questions),id_form],     
                    (tx, res) => {
                      (res.rowsAffected > 0) ? console.log("Preguntas Guardadas") : console.log("Algo Salio Mal");
                    });
                  }
              }
              else{
                txn.executeSql("UPDATE question_table SET question_array = ? WHERE form_id = ?",[JSON.stringify(questions),id_form],     
                (tx, res) => {
                  (res.rowsAffected > 0) ? console.log("Preguntas del formulario N "+id_form+" Actualizadas") : console.log("Algo Salio Mal");
                });
              }
            });
          });
        }
        }else{
           db.transaction(txn => {
            txn.executeSql(
            "SELECT * from question_table WHERE form_id = ? ",[id_form],     
            function(tx, res) {
              if(res.rows.length > 0){
                    txn.executeSql("DELETE FROM question_table WHERE form_id = ?",[id_form],     
                    (tx, res) => {
                      (res.rowsAffected > 0) ? console.log("Eliminando") : console.log("Algo Salio Mal");
                    });
              }
            });
          });
        }
      });   
  }

  _populateParticipants(){
    let communities = this.state.user[0].communities;
    if(communities != null){
        //C40
        communities.split("@@").map((opcion, index, array) => {
        var opciones = opcion.split(";");
        var community = opciones[0];
        var form_id = 4;
        var form_code = "C40";
        var formData = new FormData();
        formData.append("community", community);
        formData.append("form_id", form_id);
        formData.append("form_code", form_code);
        fetch(urlBase+'admin/movil/participants/json', {
          method: 'POST',
          headers: { 'Accept': 'application/json','Content-Type': 'multipart/form-data',}, 
          body: formData,
        })
          .then(response => response.json())
          .then(participants => { 
            //console.log(participants);
            if(participants.data.length > 0){
              var vacio = false;
              db.transaction(txn => {
                txn.executeSql("SELECT * from participants_table WHERE form_id = ? AND community = ?",[form_id, community],
                  function(tx, res) {
                   //console.log(res)
                    if(res.rows.length == 0){
                      vacio = true;
                      if(vacio){
                        txn.executeSql("INSERT INTO participants_table (participants_array, form_id, community) VALUES (?,?,?)",[JSON.stringify(participants),form_id,community],     
                          (tx, res) => {
                            //console.log(res)
                            if (res.rowsAffected > 0) {
                              console.log("Participantes Guardados")
                            }else {
                              console.log("Algo Salio Mal");
                            }
                          });
                      }
                    }
                    else{
                      console.log("Se actualizara")
                      txn.executeSql("UPDATE participants_table SET participants_array = ? WHERE form_id = ? AND community = ? ",[JSON.stringify(participants),form_id,community],     
                        (tx, res) => {
                          if (res.rowsAffected > 0) {
                            console.log("Participantes Actualizados")
                          } else {
                            console.log("Algo Salio Mal");
                          }
                        });
                    }
                  }   
                  );
              });
              this.setState({participants: participants.data})

            }else{
              let participants = {data: [{'id': 'xxxxxxxxxxx', 'name_participant': no_participant}]};
              this.setState({participants: participants.data})
            }                    
          });

      });

      //C41
      communities.split("@@").map((opcion, index, array) => {
        var opciones = opcion.split(";");
        var community = opciones[0];
        var form_id = 5;
        var form_code = "C41";
        var formData = new FormData();
        formData.append("community", community);
        formData.append("form_id", form_id);
        formData.append("form_code", form_code);

        fetch(urlBase+'admin/movil/participants/json', {
          method: 'POST',
          headers: { 'Accept': 'application/json','Content-Type': 'multipart/form-data',}, 
          body: formData,
        })
          .then(response => response.json())
          .then(participants => { 
            if(participants.data.length > 0){
              var vacio = false;
              db.transaction(txn => {
                txn.executeSql("SELECT * from participants_table WHERE form_id = ? AND community = ?",[form_id, community],
                  function(tx, res) {

                    if(res.rows.length == 0){
                      vacio = true;
                      if(vacio){
                        txn.executeSql("INSERT INTO participants_table (participants_array, form_id, community) VALUES (?,?,?)",[JSON.stringify(participants),form_id,community],     
                          (tx, res) => {
                           // console.log("Insertado");
                            if (res.rowsAffected > 0) {
                              console.log("Participantes Guardados")
                            }else {
                              console.log("Algo Salio Mal");
                            }
                          });
                      }
                    }
                    else{
                      console.log("Se actualizara")
                      txn.executeSql("UPDATE participants_table SET participants_array = ? WHERE form_id = ? AND community = ?",[JSON.stringify(participants),form_id, community],     
                        (tx, res) => {
                          if (res.rowsAffected > 0) {
                            console.log("Participantes Actualizados")
                          } else {
                            console.log("Algo Salio Mal");
                          }
                        });
                    }
                  }   
                  );
              });
              this.setState({participants: participants.data})

            }else{
              let participants = {data: [{'id': 'xxxxxxxxxxx', 'name_participant': no_participant}]};
              this.setState({participants: participants.data})
            }                    
          });

      });

      //C42
      communities.split("@@").map((opcion, index, array) => {
        var opciones = opcion.split(";");
        var community = opciones[0];
        var form_id = 6;
        var form_code = "C42";
        var formData = new FormData();
        formData.append("community", community);
        formData.append("form_id", form_id);
        formData.append("form_code", form_code);
        fetch(urlBase+'admin/movil/participants/json', {
          method: 'POST',
          headers: { 'Accept': 'application/json','Content-Type': 'multipart/form-data',}, 
          body: formData,
        })
          .then(response => response.json())
          .then(participants => { 
            if(participants.data.length > 0){
              var vacio = false;
              db.transaction(txn => {
                txn.executeSql("SELECT * from participants_table WHERE form_id = ? AND community = ?",[form_id, community],
                  function(tx, res) {
                    if(res.rows.length == 0){
                      vacio = true;
                      if(vacio){
                        txn.executeSql("INSERT INTO participants_table (participants_array, form_id, community) VALUES (?,?,?)",[JSON.stringify(participants),form_id, community],     
                          (tx, res) => {
                            if (res.rowsAffected > 0) {
                              console.log("Participantes Guardados")
                            }else {
                              console.log("Algo Salio Mal");
                            }
                          });
                      }
                    }
                    else{
                      console.log("Se actualizara")
                      txn.executeSql("UPDATE participants_table SET participants_array = ? WHERE form_id = ? AND community = ?",[JSON.stringify(participants),form_id,community],     
                        (tx, res) => {
                          if (res.rowsAffected > 0) {
                            console.log("Participantes Actualizados")
                          } else {
                            console.log("Algo Salio Mal");
                          }
                        });
                    }
                  }   
                  );
              });
              //this.setState({participants: participants.data})

            }else{
              let participants = {data: [{'id': 'xxxxxxxxxxx', 'name_participant': no_participant}]};
              //this.setState({participants: participants.data})
            }                    
          });

      });
    }

  }

  _setWorkMethod(isOrNot){
    db.transaction(txn => {
        txn.executeSql(
          "SELECT * FROM connection_table",[],     
          function(tx, res) {
            if(res.rows.length == 0){
              console.log("Se insertara el nuevo estado de conexion default");
             try {
              txn.executeSql("INSERT INTO connection_table (isOffline) VALUES (?)",[isOrNot],     
              async (tx, res) => {
                await AsyncStorage.setItem('isOffline', isOrNot);
              });
             } catch(e) {
               console.log(e);
             }
            }
            if (res.rows.length == 1) {
              console.log("Se actualizo el nuevo estado de conexion");
            try {
              txn.executeSql("SELECT * from connection_table",[],     
              async (tx, res) => {
                txn.executeSql("UPDATE connection_table SET isOffline = ? WHERE id = ?", [isOrNot,res.rows._array[0].id]);
                await AsyncStorage.setItem('isOffline', isOrNot);
              });
             }catch(e) {
               console.log(e);
             }
            }
        });
    });
  }

  _setButtonState(){
    console.log("Se establecera el estado de conexion")
      db.transaction(tx => {
        tx.executeSql('SELECT * FROM connection_table', [], 
          (tx, results) => {
            console.log(results);
            if(results.rows.length > 0){
              if(results.rows._array[0].isOffline == "FALSE"){
                console.log("Trabajando con internet")
                this.setState({desconectar:false});
              }
              else{
                 console.log("Trabajando sin internet")
                this.setState({desconectar:true});
              }
            }
            else{
              console.log("Trabajando con internet")
              this.setState({desconectar:false});
            }
          });
      });
    }
  
  

  _showCommunities = () => {
    this.props.navigation.navigate('Communities', {partner_id: this.state.user[0].id});
  };

  _showRegister = () => {
    this.props.navigation.navigate('Register');
  };

  _showMoreApp = () => {
    this.props.navigation.navigate('Other');
  };

  _signOutAsync = async () => {
    await AsyncStorage.clear();
     db.transaction(txn => {
          txn.executeSql(
          "DELETE FROM user_table",[],     
          (tx, res) => {
            (res.rowsAffected > 0) ? console.log("Usuario Eliminado") : Alert.alert('Algo ha salido mal porfavor Notificar');
          });
      });
      this.props.navigation.navigate('Auth');
  };
}
