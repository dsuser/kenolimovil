import React, { Component } from 'react';
import {Alert, Text, View, Button, StatusBar, TouchableOpacity, AsyncStorage, ActivityIndicator, ScrollView, KeyboardAvoidingView, Platform} from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import { Card } from 'react-native-elements'
import md5 from 'md5';
import styles from '../styles/styles';
import * as SQLite from 'expo-sqlite';
import RNRestart from 'react-native-restart';
import { NavigationActions, StackActions } from 'react-navigation';
import Icon from "react-native-vector-icons/MaterialIcons";

//Elementos Genericos
import GenericSelector    from '../components/GenericSelector';
import Calculation        from '../functions/CalculationFuntions';
import Empty              from '../functions/EmptyFunctions';
import Condition          from '../functions/ConditionFunctions';

//Url del Rest
import urlBase            from '../enviroment/url';
//Base de datos local

const db = SQLite.openDatabase('answersData.db');
const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'Home' })],
});

var resultado     = "";
var num_person    = 0;
var not_saved     = 0;
var miembros      = [];
var miembro       = {};
var member_area   = 0;
var genero_aux    = "";
var old_orders    = [];

//Nuevas variables de uso
var numero_persona = 0;
var numero_miembro = 0;
var respuestas = [];
export default class OtherScreen extends Component {
	 static navigationOptions = {
    	title: 'Fundacion Kenoli',
  	};

  	constructor(props){
  		super(props);
  		this.state = {
        url_gateway: urlBase+'log/gateway/input',
        url: urlBase+'admin/movil/json',
  			data: undefined,
  			question: [],
        loading: true,
        resultado: [],
        answers: [],
        order:0,
        old_order: 0,    //Establecer a 0 //pruebas 8
        is_person: 0,    //Establecer a 0 //pruebas 2
        notsaved: 0,     //Establecer a 0 //pruebas 2
        num_person: 1,   //Dejar en 1
        disabled: false, //estado de botones
        not_saved: true,
  		}
  	}

    componentDidMount(){
        resultado     = "";
        num_person    = 0;
        not_saved     = 0;
        miembros      = [];
        miembro       = {};
        member_area   = 0;
        genero_aux    = "";
        old_orders    = [];
        numero_persona = 0;
        numero_miembro = 0;
        respuestas  = [];
       

       this._checkConnectivity();
       this._retrieveData();
    }

    _checkConnectivity = async () => {
     let isOffline = await AsyncStorage.getItem('isOffline');
     const user = await AsyncStorage.getItem('user');
      NetInfo.fetch().then(state => {
        if(isOffline  == "TRUE"){
          console.log("No Conectado");
          this._getStoredQuestions();
        }else{
          if(state.isConnected){
            console.log("Conectado");
            this._getData(user);
          }else{
            console.log("No Conectado");
            this._getStoredQuestions();
          }
        }
      });
    }

    _retrieveData = async () => {
      try {
        const stored = await AsyncStorage.getItem('answers');
        
        if (stored !== null) {
          let answers = JSON.parse(stored);
          answers.form = this.props.navigation.getParam('code');
          answers.created = new Date();
          answers.form_id = this.props.navigation.getParam('form_id');
          let form_id = this.props.navigation.getParam('form_id');
          if(form_id >= 4 && form_id <= 6){
            answers.uuidMD5 = this.props.navigation.getParam('participant_id');
            answers.name_participant = this.props.navigation.getParam('participant_name');
          }
          this.setState({answers: answers});
        }
      }catch (error) {
        console.log(error);
      }
    }

    _getData = (user) => {
      console.log("Entro a GetData");
      console.log(user);
      var juser = JSON.parse(user);
      var user_id = juser[0].idUserLog;
      var form_id = this.props.navigation.getParam('form_id');
      console.log("enviado: " + user_id);
      let formData = new FormData();
      formData.append("form_id", form_id);
      formData.append("user_id", user_id);
      fetch(this.state.url, {
        method: 'POST',
        headers: { 'Accept': 'application/json','Content-Type': 'multipart/form-data',}, 
        body: formData,
      })
      .then(response => response.json())
      .then(questions => {
        console.log(questions);
        if(questions.data.length == 0)
        {
          db.transaction(txn => {
            txn.executeSql(
            "SELECT * from question_table WHERE form_id = ? ",[form_id],     
            function(tx, res) {
              if(res.rows.length > 0){
                vacio = false;
                  if(vacio){
                    txn.executeSql("DELETE FROM question_table WHERE form_id = ?",[form_id],     
                    (tx, res) => {
                      (res.rowsAffected > 0) ? console.log("Eliminando") : console.log("Algo Salio Mal");
                    });
                  }
              }
            });
          });
          this.setState({loading:false})
        }
        else{
          if(questions != []){
                              var vacio = false;
                               db.transaction(txn => {
                                txn.executeSql(
                                  "SELECT * from question_table WHERE form_id = ? ",[form_id],     
                                  function(tx, res) {
                                    if(res.rows.length == 0){
                                      vacio = true;
                                      if(vacio){
                                        txn.executeSql("INSERT INTO question_table (question_array, form_id) VALUES (?,?)",[JSON.stringify(questions),form_id],     
                                        (tx, res) => {
                                          //console.log('Results', res.rowsAffected);
                                          if (res.rowsAffected > 0) {
                                           console.log("Preguntas Guardadas")
                                          } else {
                                           console.log("Algo Salio Mal");
                                          }
                                        });
                                      }
                                    }
                                    else{
                                      console.log("Se actualizara")
                                        txn.executeSql("UPDATE question_table SET question_array = ? WHERE form_id = ?",[JSON.stringify(questions),form_id],     
                                        (tx, res) => {
                                         console.log('Results', res.rowsAffected);
                                          if (res.rowsAffected > 0) {
                                            console.log("Preguntas Actualizadas")
                                          } else {
                                           console.log("Algo Salio Mal");
                                          }
                                        });
                                    }
                                  }   
                                );
                              });
                              let question_inicial = questions.data[this.state.order];
                              question_inicial != undefined ? this.setState({data:questions.data, loading:false, question:question_inicial})
                                                            : this.setState({data:questions.data, 
                                                                             loading:false, 
                                                                             question:questions.data[this.state.order+1], 
                                                                             order: parseInt(this.state.order+1)})
                            }
        }
                             
                          });
    }

    _getStoredQuestions = () => {
      console.log("Preguntas almacenados");
      var form_id = this.props.navigation.getParam('form_id');
       db.transaction(tx => {
       tx.executeSql('SELECT question_array FROM question_table WHERE form_id =? ', [form_id], 
       (tx, results) => {
        if(results.rows.length == 0){
          this.setState({loading:false})
        }else{
          console.log(results)
          let questions = JSON.parse(results.rows._array[0]["question_array"]);
          let question_inicial = questions.data.find(data => data['orden'] == this.state.order);
                              question_inicial != undefined ? this.setState({data:questions.data, loading:false, question:question_inicial})
                                                            : this.setState({data:questions.data, 
                                                                             loading:false, 
                                                                             question:questions.data[this.state.order+1], 
                                                                             order: parseInt(this.state.order+1)})

        }
       });
      });
    }

    _validate  = (question) => {
      switch (question['type_result']) {
        case "0": return Empty.textNoEmpty(resultado);    break;
        case "1": return Empty.dateNoEmpty(resultado);    break;
        case "2": return Empty.numericNoEmpty(resultado); break;
        case "3": return Empty.numericNoEmpty(resultado); break;
        case "4": return Empty.optionNoEmpty(resultado);  break;
        case "5": return Empty.optionNoEmpty(resultado);  break;
        case "9": return true;                            break;
        default : console.log("Vacio Default");           break;
      }
    }

    _condicionated = (question) => {
        switch (question['type_result']) {
          case "1": return Condition.dateCondition(resultado,question);     break;
          case "2": return Condition.integerCondition(resultado,question);  break;
          case "3": return Condition.decimalCondition(resultado,question);  break;
          case "4": return Condition.simpleCondition(resultado,question);   break;
          case "5": return Condition.multipleCondition(resultado,question); break;
          case "9": return false;                                           break;
          default : console.log("Condicion Default");                       break;
        }
    }

    

    _NextQuestion = async () => {
      if(this._validate(this.state.question)){
        console.log("valida");
        if(Condition.questionCondition(this.state.question)){
          console.log("pregunta condicionada");
          if(this._condicionated(this.state.question)){
            if(Condition.jumpCondition(resultado,this.state.question)){
              console.log("Pregunta con salto");
              this._yesJump();
              this._addInfo();
             
            }else{
              console.log("pregunta sin salto");
              this._noJump();
              this._addInfo();
            }
          }
        }else{
          console.log("pregunta no condicionada");
          if(this.state.question['type_result'] == 1 && (genero_aux == 1 || genero_aux == 2)){
          let actual = new Date();
          let edad = Calculation.calculateAge(resultado, actual);
          if(this.state.question['orden'] == 43.1 && genero_aux == 1 && this.props.navigation.getParam('code') == "C30"){
            if(edad < 5){
              this._dateJump(43.2);
              this._addMemberData(resultado,this.state.question);
            }else{
              this._dateJump(43.5);
              this._addMemberData(resultado,this.state.question);
            }
          }
          if (this.state.question['orden'] == 44.1 && genero_aux == 2) {
            if(edad < 5){
              this._dateJump(44.2);
              this._addMemberData(resultado,this.state.question);

            }
            if(edad > 5 && edad <= 11){
              this._dateJump(44.5);
              this._addMemberData(resultado,this.state.question);
            }
            if(edad >=12){
              this._dateJump(45);
              this._addMemberData(resultado,this.state.question);
            }
          }
          resultado = "";
        }else{
          this._noJump();
          this._addInfo();
        }
        }

      }
      else{
        console.log("no valida");
        resultado = "";
        return;
      }
  }

    _BackQuestion = async () => {
      let ultima_contestada = old_orders[old_orders.length-1];
      let u_question = this.state.data.find(data=>data['orden'] == ultima_contestada);
      console.log(u_question['title_question']);
      switch (this.props.navigation.getParam('code')) {
        case "C30":
        if(u_question['title_question'].trim() == "Ingrese el nombre del o la participante principal"){
          numero_persona = 0;
        }
        break;
        case "C31":
        if(u_question['title_question'].trim() == "Ingrese el nombre del Grupo o la Escuela participante"){
          numero_persona = 0;
        }
        break;
        case "C32":
        if(u_question['title_question'].trim() == "Ingrese el nombre de la familia o del grupo comunitario"){
          numero_persona = 0;
        }
        break;
        default:
          console.log("Default");
        break;
      }
      resultado = respuestas[ultima_contestada];
      delete respuestas[ultima_contestada];
      this.setState(prevState =>({
        question: prevState.data.find(data=>data['orden'] == ultima_contestada),
        order: prevState.data.findIndex(data=>data['orden'] == ultima_contestada)
      }));
      old_orders.pop();
    }

    _yesJump = () =>{
      if(this.state.data.find(data => data['id'] == this.state.question['next_question'])!=undefined){
        this.setState(prevState =>({ 
                    question: prevState.data.find(data => data['id'] == this.state.question['next_question']),
                    order: prevState.data.findIndex(data => data['id'] == this.state.question['next_question'])
        }));
      }
      else{
        let newId = parseInt(this.state.question['next_question'])+1;
        if(this.state.data.find(data => data['id'] == newId)==undefined){

          let orden =  this.state.order; //ultimo indice
          let lastkpi = parseInt(this.state.question['is_kpi']); //ultimo kpi evaluado

          while(this.state.data[orden+1] != undefined &&  this.state.data[orden+1]['is_kpi'] == lastkpi ){
            orden++;
          }
          orden++;
           this.setState(prevState =>({ 
                    question: prevState.data[orden],
                    order: orden
            }));
         
        }
       
      }
      
    }
    _noJump = () => {
      this.setState(prevState =>({ 
          question: prevState.data[prevState.order+1],
          order: prevState.order + 1 
      }));
    }

    _dateJump = (orden) => {
      this.setState(prevState =>({ 
                    question: prevState.data.find(data => data['orden'] == orden),
                    order: prevState.data.findIndex(data => data['orden'] == orden)
      }));
    }

    _addInfo = () => {
      (numero_persona <= 1) ? this._addPersonData(resultado,this.state.question,this.state.answers) : this._addMemberData(resultado,this.state.question);
      old_orders.push(this.state.question['orden']);
      resultado = "";
    }

    _makeUUID = (cadena) =>{
      let uuid = cadena.substring(0,8)+'-'+cadena.substring(8,12)+'-'+cadena.substring(12,16)+'-'+cadena.substring(16,20)+'-'+cadena.substring(20);
      return uuid;
    }

    _addPersonData = (resultado,question,answers) => {
      switch (this.props.navigation.getParam('code')) {
        case "C30":
        case "C31":
        case "C32":
          if(numero_persona == 0 && question['is_person'] != 1){ //Si no hay mas participantes solo se ejecuta esto
            answers[question['orden']] = resultado;
          }
          if(question['is_person'] == 1){
            numero_persona++;
          }
          if(question['is_person'] == 1 && numero_persona == 1){
            answers['uuidMD5'] = this._makeUUID(md5(Math.random()*Number.MAX_VALUE + (Math.random()*10)));
            answers[question['orden']] = resultado;
          }else if (question['is_persona'] != 1 && numero_persona == 1) {
            answers[question['orden']] = resultado;
          } //Aca garantizo que se han agregado solo los datos de participante
          if(numero_persona > 1){
            this._addMemberData(resultado,question);
          }
          respuestas = answers;
          console.log(answers);
        break;
        case "C40":
        case "C41":
        case "C42":
          if(numero_persona == 0 && question['is_person'] != 1){ //Si no hay mas participantes solo se ejecuta esto
            answers[question['orden']] = resultado;
          }
          if(question['is_person'] == 1){
            numero_persona = 2;
          }
          if(question['is_person'] == 1 && numero_persona == 1){
            answers[question['orden']] = resultado;
          }else if (question['is_persona'] != 1 && numero_persona == 1) {
            answers[question['orden']] = resultado;
          } //Aca garantizo que se han agregado solo los datos de participante 1
          if(numero_persona > 1){
            this._addMemberData(resultado,question); //Ingreso de datos de miembro
          }
          respuestas = answers;
        break;
        default:
          console.log("Ingreso de datos para personas participantes");
        break;
      }
      
     
    }

    _addMemberData = (resultado,question) => {
      switch (this.props.navigation.getParam('code')) {
        case "C30":
          if(this.state.question['title_question'] == "Ingrese el sexo del o la miembro de la familia."){
            genero_aux = resultado;
          }
          if(question['title_question'].trim() == "¿Es necesario agregar un miembro más de esta familia?"){
            miembros.push(miembro);
            miembro = {};
          }
          miembro[question['orden']] = resultado;
        break;
        case "C31":
          if(question['title_question'].trim() == "¿Es necesario agregar un miembro más del Grupo?"){
             miembros.push(miembro);
             miembro = {};
          }
          if(question['title_question'].trim() == "¿Es necesario agregar un miembro más del Grupo?" && resultado == 2){
             numero_persona--;
          }
          miembro[question['orden']] = resultado;
        break;
        case "C32":
          if(question['title_question'].trim() == "¿Hay algún otro miembro de esta familia, que también va a participar en la iniciativa económica?"){
             miembros.push(miembro);
             miembro = {};
          }
          if(question['title_question'].trim() == "¿Hay algún otro miembro de esta familia, que también va a participar en la iniciativa económica?" && resultado == 2){
             numero_persona--;
          }
          miembro[question['orden']] = resultado;
        break;
        case "C40":
          if(question['title_question'].trim() == "¿Es necesario agregar una persona más de esta familia?"){
             miembros.push(miembro);
             miembro = {};
          }
          if(question['title_question'].trim() == "¿Es necesario agregar una persona más de esta familia?" && resultado == 2){
             numero_persona--;
          }
          miembro[question['orden']] = resultado;
        break;
        default:
          console.log("Si ve este mensaje es porque entro a C41 o C42 y no hay datos de miembro");
        break;
      }
      
    }

  _getValue(value){
      resultado = value;
  }

  _restartScreen = () => {
    RNRestart.Restart();
   }

  _SendData = () => {
    console.log("enviar....")
    this.setState(prevState =>({ 
      disabled: !prevState.disabled 
    }));
    let answers = respuestas;
    answers.persons = miembros;
    
    var formData = new FormData();
    formData.append("answers", JSON.stringify(answers));
    fetch(this.state.url_gateway, {
      method: 'POST',
      headers: { 'Accept': 'application/json','Content-Type': 'multipart/form-data',}, 
      body: formData,
    })
     .then(response => response.json())
     .then(respuesta => {
        if(respuesta.ok){
          Alert.alert('Informacion!!',''+respuesta.msg,
                        [{ text: 'Ok', onPress: () => {this._restartScreen; this.props.navigation.navigate('Home')}},],
                        { cancelable: false });}
        else{
          Alert.alert('Informacion!!',''+respuesta.msg,
                      [{ text: 'Ok', onPress: () => {this.setState({disabled:false});this.props.navigation.navigate('Home')}},],
                       { cancelable: false }); 
        }})
      .catch((error) => { console.log(error);});
   }


  _SaveData = (fail) => {
    let answers = respuestas;
    answers.persons = miembros;
    let code = this.props.navigation.getParam('form_id');


    db.transaction(txn => {
      txn.executeSql(
        "INSERT INTO answer_table (code_form, answer_array) VALUES (?,?)",[code, JSON.stringify(answers)],     
        (tx, res) => {
          console.log("Fallo: " + fail);
          console.log('Results', res.rowsAffected);
                if (res.rowsAffected > 0) {
                  if(fail) {
                    Alert.alert(
                      'Datos no enviados porfavor intente nuevamente mas tarde!!',
                      'Sus resultados han sido almacenados en este dispositivo puede verlos en Cuestionarios Guardados',
                      [ { text: 'Ok', onPress: () => {this._restartScreen; this.props.navigation.navigate('Home');}},],
                      { cancelable: false });
                  }else{
                    Alert.alert('Informacion!!','Datos almacenados exitosamente',
                      [{ text: 'Ok', onPress: () => {this._restartScreen; this.props.navigation.navigate('Home');}},],
                      { cancelable: false });
                  }
                } else {
                  Alert.alert('Algo ha salido mal');
                }
        });
    });
  }

  render() {
    if(this.state.loading){
      return(
        <View style={[styles.loadingContainer, styles.centerContainer]}> 
        <ActivityIndicator size="large" color="#0000ff" />
        <Text>Cargando...</Text>
        </View>
        );
    }
    else{
      if(this.state.data == undefined){
        return(
        <View style={[styles.loadingContainer, styles.centerContainer]}>
        <Card containerStyle={[{ borderRadius: 20, elevation: 0, height: 225 }]} title='Este cuestionario no aplica para esta Organización. No tiene  preguntas disponibles.' image={require('../assets/images/notfound.png')}>
        </Card> 
        </View>
        );
      }else{
        let fail = false;
      let str_linea_base = "Gracias por completar este cuestionario de Línea Base.";
      let str_monitoreo = "Gracias por completar este cuestionario de Monitoreo.";
      let base = [(this.state.question['title_question'].trim()).toLowerCase(),str_linea_base.toLowerCase(),str_monitoreo.toLowerCase()];
      let button;
      if( base[0] == base[1] || base[0] == base[2]){
        let net = NetInfo.fetch();
        let status = net["_55"]["isConnected"]
        if(status){
          button =
          <View style={styles.controlButtons}>
          <TouchableOpacity style={this.state.disabled ?  [styles.buttonDisabled] : [styles.buttonNext, styles.sendButtons]} onPress={()=>this._SendData()} disabled={this.state.disabled}>
          <Text style={styles.buttonText} >Enviar  <Icon name={"send"} size={20} color={'rgb(255, 255,255)'}/></Text>
          </TouchableOpacity>
          <TouchableOpacity style={this.state.disabled ?  [styles.buttonDisabled] : [styles.buttonNext, styles.savedButtons]} onPress={()=>this._SaveData(fail)} disabled={this.state.disabled}>
          <Text style={styles.buttonText} ><Icon name={"save"} size={20} color={'rgb(255, 255,255)'}/>  Guardar</Text>
          </TouchableOpacity>
          </View>
        }else{
          button =
          <View style={styles.controlButtons}>
          <TouchableOpacity style={!(this.state.disabled) ?  [styles.buttonDisabled] : [styles.buttonNext, styles.sendButtons]} onPress={()=>this._SendData()} disabled={!(this.state.disabled)}>
          <Text style={styles.buttonText} >Enviar</Text>
          </TouchableOpacity>
          <TouchableOpacity style={this.state.disabled ?  [styles.buttonDisabled] : [styles.buttonNext, styles.savedButtons]} onPress={()=>this._SaveData(fail)} disabled={this.state.disabled}>
          <Text style={styles.buttonText} >Guardar</Text>
          </TouchableOpacity>
          </View>
        }

      }
      else{
        if(this.state.question['orden'] != 0){
          button = 
          <View style={styles.controlButtons}>
          <TouchableOpacity style={this.state.disabled ?  [styles.buttonDisabled] : [styles.buttonNext, styles.buttonColor]}  onPress={()=>this._BackQuestion()} disabled={this.state.disabled}>
          <Text style={styles.buttonText} >Anterior  <Icon name={"undo"} size={30} color={'rgb(255, 255,255)'}/></Text>
          </TouchableOpacity>
          <TouchableOpacity style={this.state.disabled ?  [styles.buttonDisabled] : [styles.buttonNext, styles.buttonColor]}  onPress={()=>this._NextQuestion()} disabled={this.state.disabled}>
          <Text style={styles.buttonText} ><Icon name={"redo"} size={30} color={'rgb(255, 255,255)'}/> Siguiente </Text>
          </TouchableOpacity>
          </View>
        }
        else{
          button =
          <TouchableOpacity style={this.state.disabled ?  [styles.buttonDisabled] : [styles.buttonNext, styles.buttonColor]}  onPress={()=>this._NextQuestion()} disabled={this.state.disabled}>
          <Text style={styles.buttonText} ><Icon name={"redo"} size={30} color={'rgb(255, 255,255)'}/> Siguiente </Text>
          </TouchableOpacity>
        }
      }
      let input = <GenericSelector question = {this.state.question} _getValue = {this._getValue} resultado = {resultado} />;
      return (
        <View>
        <View style = {styles.containerNext}>
        {button}
        </View>
        <KeyboardAvoidingView style={styles.questionContainer} behavior="position">
        <View key={Math.random()}>
        {input}
        </View>
        <View style={styles.viewboton}>    
        <StatusBar barStyle="default" />
        </View>
        </KeyboardAvoidingView>
        </View>
        );
      }
      
    }
  }
}
