import React, { Component } from 'react';
import { View, ScrollView, AsyncStorage, ActivityIndicator, Text, Button, StatusBar} from 'react-native';
import {YellowBox} from 'react-native';
//import { Container, Header, Content, Footer, FooterTab, Left, Right, Body, Item } from 'native-base';
import NetInfo from "@react-native-community/netinfo";
import * as SQLite from 'expo-sqlite';
import styles from '../styles/styles';
import Accordian from '../components/Accordian';
import urlBase            from '../enviroment/url';
const db = SQLite.openDatabase('answersData.db');
export default class QuestionnaireScreen extends Component {
	static navigationOptions = {
    	title: 'Ingreso de cuestionarios',
  	};
  	constructor(props){
  		super(props);
  		this.state={
  			url: urlBase+'admin/movil/forms/json',
  			loading: true,
        isOffline: "",
	  		menu :[
		        // {
		        //   id: 1, 
		        //   title: 'Linea Base', 
		        //   data: [
		        //   	{key: 1, name: 'LB Registro de participantes individuales', value:'1', code: 'C30'},
		        //   	{key: 2, name: 'LB Registro de participantes grupales', value:'2', code: 'C31'},
		        //   	{key: 3, name: 'LB Iniciativas Económicas Familiares', value:'3', code: 'C32'}
		        //   ]
		        // },
		        // { 
		        //   id: 2,	
		        //   title: 'Monitoreo',
		        //   data: [
		        //   	{key: 4, name: 'Monitoreo, Familias individuales', value:'4', code: 'C40'},
		        //   	{key: 5, name: 'Monitoreo, Participantes Grupales', value:'5', code: 'C41'},
		        //   	{key: 6, name: 'Monitoreo, Iniciativas Económicas Individuales', value:'6', code: 'C42'}
		        //   ]
		        // }
	      		]
  	    }
  	}

  	componentDidMount(){
      this._checkConnectivity();
  	}

  	_checkConnectivity = async () => {
     let isOffline = await AsyncStorage.getItem('isOffline');
      NetInfo.fetch().then(state => {
        if(isOffline  == "TRUE"){
          console.log("No Conectado");
          this._getStoredForms();
        }else{
          if(state.isConnected){
            console.log("Conectado");
            this._getData();
          }else{
            console.log("No Conectado");
            this._getStoredForms();
          }
        }
      });
    }
   
    _getData = () => {
      // console.log("Entro a GetData")
      var form_id = 1;
      //var form_id = this.props.navigation.getParam('form_id');
      //let formData = new FormData();
      //formData.append("form_id", form_id);
      fetch(this.state.url, {
        method: 'POST',
        headers: { 'Accept': 'application/json','Content-Type': 'multipart/form-data',}, 
        //body: formData,
      })
      .then(response => response.json())
      .then(forms => {
                             if(forms != []){
                              var vacio = false;
                               db.transaction(txn => {
                                txn.executeSql(
                                  "SELECT * from kenolif_table where id = ?",[form_id],     
                                  function(tx, res) {
                                    if(res.rows.length == 0){
                                      vacio = true;
                                      if(vacio){
                                        txn.executeSql("INSERT INTO kenolif_table (forms_array) VALUES (?)",[JSON.stringify(forms)],     
                                        (tx, res) => {
                                          //console.log('Results', res.rowsAffected);
                                          if (res.rowsAffected > 0) {
                                           console.log("Formularios Guardados")
                                          } else {
                                           console.log("Algo Salio Mal");
                                          }
                                        });
                                      }
                                    }
                                    else{
                                      console.log("Se actualizara")
                                        txn.executeSql("UPDATE kenolif_table SET forms_array = ? WHERE id = ?",[JSON.stringify(forms),form_id],     
                                        (tx, res) => {
                                         //console.log('Results', res.rowsAffected);
                                          if (res.rowsAffected > 0) {
                                            console.log("Formularios Actualizados")
                                          } else {
                                           console.log("Algo Salio Mal");
                                          }
                                        });
                                    }
                                  }   
                                );
                              });
                               this._makeMenu(forms);
                            }
                          });
    }

    _getStoredForms = () => {
       var form_id = 1;
       db.transaction(tx => {
       tx.executeSql('SELECT forms_array FROM kenolif_table WHERE id =? ', [form_id], 
       (tx, results) => {
         let forms = JSON.parse(results.rows._array[0]["forms_array"]);
         this._makeMenu(forms);
       });
      });
    }

    _makeMenu = (forms) =>{
    	let menu = [];
                               let C3_content = {};
                               let C4_content = {};
                               
                               let menuC3 = [];
                               let menuC4 = [];
                               forms.data.map((element, index) => {
                               	if(element.code_form.indexOf("C3")!= -1){
                               		let menu_item = {};
                               		menu_item.key =  index+1;
                               		menu_item.name = element.fullname_form;
                               		menu_item.value = index+1;
                               		menu_item.code = element.code_form;
                               	//	console.log(menu_item);
                               		menuC3.push(menu_item);
                               	}
                               	if(element.code_form.indexOf("C4")!= -1){
                               		let menu_item = {};
                               		menu_item.key =  index+1;
                               		menu_item.name = element.fullname_form;
                               		menu_item.value = index+1;
                               		menu_item.code = element.code_form;
                               		//console.log(menu_item);
                               		menuC4.push(menu_item);
                               	}
                               });
                               C3_content.id = 1;
                               C3_content.title = "Linea Base";
                               C3_content.data = menuC3;
                               C4_content.id = 2;
                               C4_content.title = "Monitoreo";
                               C4_content.data = menuC4;
                               menu.push(C3_content);
                               menu.push(C4_content);

                               this.setState({menu:menu, loading:false})
    }

	render() {
		console.disableYellowBox = true;
		if(this.state.loading){
      return(
        <View style={[styles.loadingContainer, styles.centerContainer]}> 
        <ActivityIndicator size="large" color="#0000ff" />
        <Text>Cargando Formularios...</Text>
        </View>
        );
    }else{
		return (
			<ScrollView>
			{ this._renderAccordians() }
			</ScrollView>
		);
	}
	}

	_renderAccordians = () => {
	     const items = [];
	     for (var item of this.state.menu) 
	     {
	          items.push(<Accordian key={item.id} title = {item.title} data =  {item.data} navigation ={this.props.navigation} />);
	     }
	     return items;
     }

	_signOutAsync = async () => {
		await AsyncStorage.clear();
    	this.props.navigation.navigate('Auth');
  	};
}

