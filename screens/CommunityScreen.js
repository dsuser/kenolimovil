import React, { Component } from 'react';
import { Alert, Text, ActivityIndicator, View, AsyncStorage, Button, ScrollView, TouchableOpacity, TouchableHighlight, Dimensions} from 'react-native';
import { Card, ListItem, Icon } from 'react-native-elements'
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import NetInfo from "@react-native-community/netinfo";
import * as SQLite from 'expo-sqlite';
const db = SQLite.openDatabase('answersData.db');
import styles from '../styles/styles';
const win = Dimensions.get('window');
const now = new Date();
var resultado;
export default class HomeScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      user: '',
      communities: '',
    }
  }
  static navigationOptions = {
    title: 'Seleccione Comunidad',
  };

  componentDidMount(){
     this._checkConnectivity();
  }


  _checkConnectivity = (answers) => {
     
    NetInfo.fetch().then(state => {
      if(state.isConnected){
        console.log("Conectado");
        this._retrieveData();
      }else{
        console.log("No Conectado");
        this._getStoredUser();
        
      }
    });
    }

  _retrieveData = async () => {
  try {
    const user = await AsyncStorage.getItem('user');
      if (user !== null) {
       this.setState({user: JSON.parse(user)});
      }
    } catch (error) {
      console.log(error);
    }
  };

  _getStoredUser = () => {
     db.transaction(tx => {
       tx.executeSql('SELECT user_string FROM user_table', [], 
       (tx, results) => {
        let user = JSON.parse(results.rows._array[0]["user_string"]);
        console.log(user);
        this.setState({user: user});
       });
      });
  }

  _storeData = async (comunity) => {
      try {
        let answers = { "com": comunity, "uuid": this.state.user[0].uri_id };
        await AsyncStorage.setItem('answers', JSON.stringify(answers));
      } catch (error) {
        console.log(error);
      }
  };

  _handlePress(selected)
  {
    resultado = selected;
  }
  render() {
    if(this.state.user == ''){
      return(
                <View style={[styles.loadingContainer, styles.centerContainer]}> 
                  <ActivityIndicator size="large" color="#0000ff" />
                  <Text>Cargando...</Text>
                </View>
            );
    }
    else{
      let communities = this.state.user[0].communities;
      if(communities != null){
        let radio_props = [];
        communities.split("@@").map((opcion, index, array) => {
        let opciones = opcion.split(";");
        let ops = {label: opciones[1], value: opciones[0]}
        radio_props.push(ops);
      }); 
      return (
        <ScrollView>

          <View style={{ padding: 10, paddingBottom: 0 }}>
              <View style = {styles.containerNext}>
                      <TouchableOpacity style={[styles.buttonNext, styles.buttonColor]} onPress={()=>this._showQuestionnaire()}>
                        <Text style={styles.buttonText} >Continuar</Text>
                      </TouchableOpacity>
              </View>
          </View>
          <View>
          <Card containerStyle={[{ borderRadius: 20, elevation:0, height: win.height - 208}]} title="Seleccione Comunidad" >
           <RadioForm
            radio_props={radio_props}
            initial={null}
            borderWidth={4}
            buttonSize={13}
            onPress={(value) => {this._handlePress(value)}}
          />
         </Card>
          </View>
          
          
        </ScrollView>
      );

      }
      else{
        resultado = "xxxxxxxxxx";
        return(
          <ScrollView>
          
           <View style={{ padding: 10, paddingBottom: 0 }}>
              <View style = {styles.containerNext}>
                      <TouchableOpacity style={[styles.buttonNext, styles.buttonColor]} onPress={()=>this._showQuestionnaire()}>
                        <Text style={styles.buttonText} >Continuar</Text>
                      </TouchableOpacity>
              </View>
              <Text style={[{textAlign: 'center', fontSize: 18}]} >
                  Este usuario no posee comunidades
              </Text>
          </View>
          
          </ScrollView>
        );

      }


      
    }
  }

  _showQuestionnaire = () => {
    if(resultado!=undefined){
      console.log(resultado);
      this._storeData(resultado);
      this.props.navigation.navigate('Questionnaire',{partner_id: this.state.user[0].id});
      resultado = undefined;
    }else{
      Alert.alert('Atencion!!','Debe seleccionar una comunidad');
    }
  };

  _signOutAsync = async () => {
    await AsyncStorage.clear();
    this.props.navigation.navigate('Auth');
  };
}
