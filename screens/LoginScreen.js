import React, { Component } from 'react';
import { Alert, AsyncStorage, Button, ImageBackground, Image, Text, TextInput,TouchableHighlight, View, KeyboardAvoidingView } from 'react-native';
//styles
import styles from '../styles/styles';
import Icon from "react-native-vector-icons/MaterialIcons";
//Url del Rest
import urlBase from '../enviroment/url';
import * as SQLite from 'expo-sqlite';
const db = SQLite.openDatabase('answersData.db');

const versionDate = new Date("2020/11/23");
const version = "V4.8"
var now = new Date();
const options = { year: 'numeric', month: 'long', day: 'numeric' };

export default class LoginScreen extends Component {
	
	static navigationOptions = {
		title: 'Fundacion Kenoli'
	};

	constructor(props){
		super(props);
		this._handlePress= this._handlePress.bind(this);
		this.state = {
			url: urlBase+'admin/movil/user/login/json',
			username: '',
			password: '',
			icon: "remove-red-eye",
			hidden: true,
			loggeado: false,
		}

	}

	componentDidMount(){
    this._createUserTable();
    this._verificarUser();
  }
  	_createUserTable(){
  		db.transaction(txn => {
	      	txn.executeSql(
	        	"SELECT name from sqlite_master WHERE type = 'table' AND name = 'user_table'",[],     
	        	function(tx, res) {
	          		if(res.rows.length == 0){
	            		txn.executeSql("DROP TABLE IF EXISTS user_table", []);
	            		txn.executeSql("CREATE TABLE IF NOT EXISTS user_table(id INTEGER PRIMARY KEY AUTOINCREMENT, user_string TEXT)",[]);
	          		}
	        	}	   
	      	);
    	});
  	}
  	_verificarUser(){
  		db.transaction(tx => {
  			tx.executeSql('SELECT * from user_table', [], 
  				(tx, results) => {
  					if(results.rows.length > 0){
  						this.setState({loggeado:true});
  						console.log("Usuario Registrados: " + results.rows.length);
  						this.props.navigation.navigate('App');
  					}
  				});
  		});
  	}
	_insertUser = async (user, navigation) => {
		if(this.state.loggeado==false){
			db.transaction(txn => {
				txn.executeSql("INSERT INTO user_table (user_string) VALUES (?)",[JSON.stringify(user)],     
					(tx, res) => {
						console.log('Results', res.rowsAffected);
						if (res.rowsAffected > 0) {
							navigation.navigate('App');
						} else {
							Alert.alert('Algo ha salido Porfavor Tome una captura de pantalla y Notificar');
						}
					});
			});
	    }
	}

	_storeData = async (user, navigation) => {
		  try {
		    await AsyncStorage.setItem('user', JSON.stringify(user.data));
		    this._insertUser(user.data, navigation);
		  } catch (error) {
		    console.log(error);
		  }
	};

	_handlePress(event = {}){
		
     	let username = this.state.username;
     	let password = this.state.password;
     	if(username != '' && password != ''){
		var formData = new FormData();
      	formData.append("username", username);
      	formData.append("password", password);
      	fetch(this.state.url, {
        	method: 'POST',
        	headers: { 'Accept': 'application/json','Content-Type': 'multipart/form-data',}, 
        	body: formData,
      	})
      	  .then(response => response.json())
      	  .then(user => {	
      	  	console.log(user);
                            if(user.data[0].uri_id != null){ 
                            	this._storeData(user, this.props.navigation);
                            }
                            else{ 
                            	Alert.alert('Advertencia', 'Usuario o Contraseña Incorrectos');
                            }
                        });
  		}
  		else{
  			Alert.alert('Atencion!!','Porfavor ingrese sus credenciales');
  		}
	}

	_changeIcon(){
		this.setState(prevState =>({ 
			icon: prevState.icon === 'remove-red-eye' ? 'done' : 'remove-red-eye',
			hidden: !prevState.hidden 
		}));
	}

	render() {
		return (
			<ImageBackground style={styles.imageBackground} source={require('../assets/images/fondo.jpeg')}>
					<KeyboardAvoidingView behavior="position">
					<View style={styles.container}></View>
						<View style={styles.mainContainer}>
								<View style={styles.textContainer}>
									<Text style={styles.loginText}>Inicio de Sesión</Text>
								</View>
								<View style={styles.inputContainer}>
									<TextInput style={styles.inputText}
											   placeholder = "Nombre de usuario" 
											   underlineColorAndroid='transparent'
											   onChangeText={(text) => this.setState({username:text})}
											   />
								</View>
								<View style={styles.inputContainer}>
								<TextInput style={styles.inputText}
											   placeholder = "Contraseña"
											   secureTextEntry={this.state.hidden} 
											   underlineColorAndroid='transparent'
											   onChangeText={(text) => this.setState({password:text})}
											   />
								<Icon style={styles.inconLogin} name={this.state.icon} size={30} onPress={() => this._changeIcon()} color={'rgb(153, 204,255)'}/>
								</View>

								<View style={styles.containerButtonsLogin}>
									<TouchableHighlight style={[styles.buttonLogin, styles.loginButton]} onPress={() => this._handlePress() }>
		          						<Text style={styles.buttonTextLogin}>Ingresar</Text>
		    						</TouchableHighlight>		
								</View>
								<Text style={[{color:'white'}]}>
									{version}{" "}{versionDate.getFullYear()}{("0" + (versionDate.getMonth() + 1)).slice(-2)}{("0"+versionDate.getDate()).slice(-2)}
									{"                           "}
									{now.toDateString()}
								</Text>	
								
						</View>
					</KeyboardAvoidingView>
			</ImageBackground>
		);
	}

}

