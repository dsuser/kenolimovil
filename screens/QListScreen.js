import React, { Component } from 'react';
import Accordian from '../components/Accordian';
import NetInfo from "@react-native-community/netinfo";
import {Alert, Text, View, Button, StatusBar, TouchableOpacity, AsyncStorage, ActivityIndicator, ScrollView, KeyboardAvoidingView, Platform} from 'react-native';
import * as SQLite from 'expo-sqlite';
import styles from '../styles/styles';
import RNRestart from 'react-native-restart';

const db = SQLite.openDatabase('answersData.db');
export default class QListScreen extends Component {
	constructor(props){
  		super(props);
  		this.state = {
  		data: "",
  		question: [],
        loading: true,
        resultado: [],
        previous_answers: "",
        order:0,
        old_order: 0,    //Establecer a 0 //pruebas 40
        is_person: 0,    //Establecer a 0 //pruebas 2
        notsaved: 0,     //Establecer a 0 //pruebas 2
        num_person: 1,   //Dejar en 1
        disabled: false, //estado de botones
        not_saved: true,
        id_element: props.navigation.getParam('id'),
        menu :[
		        {
		          id: 1, 
		          title: 'Participante', 
		          data: [
		          ]
		        },
		        { 
		          id: 2,	
		          title: 'Miembros',
		          data: [
		          ]
		        }
	      		]
  		}
  	}
	static navigationOptions = {
    	title: 'Respuestas Guardadas',
  	};

  	componentDidMount(){
  		this._retrieveData();
  	}

   restartScreen = () => {
    RNRestart.Restart();
   }

  	_retrieveData = async () => {
      // console.log(this.state.id_element);
      let item_id = this.props.navigation.getParam('id');
      db.transaction(txn => {
            txn.executeSql(
                "SELECT * FROM answer_table WHERE id = ?",
                [item_id],     
                (tx, res) => {
                 // console.log(res);
                     for (let i = 0; i < res.rows.length; ++i) {
                      let previous_answers = JSON.parse(res.rows.item(i).answer_array);
                      this._getStoredQuestions(previous_answers);
                    }
            });
        });
    }

    _getStoredQuestions = (previous_answers) => {
       db.transaction(tx => {
       tx.executeSql('SELECT question_array FROM question_table WHERE form_id =? ', [previous_answers.form_id], 
       (tx, results) => {
         let questions = JSON.parse(results.rows._array[0]["question_array"]);
         let menu = this.state.menu;

         for(const prop in previous_answers){
         	if(questions.data.find(data => data['orden'] == prop) != undefined){
         		if(questions.data.find(data => data['orden'] == prop)['type_result'] != 9){
         			 menu[0].data.push(questions.data.find(data => data['orden'] == prop))
         		}
         		
         	}
         	if(prop == "persons"){
            if(previous_answers[prop].length != 0){
              previous_answers[prop].map((member, index) =>{
                menu[1].data.push(member)
              });
            }
         	}
         	
         }
         this.setState({data: questions.data, previous_answers: previous_answers, menu: menu});
       });
      });
    }

  	
	render() {
		if(this.state.data == ""){
			return(
				<View style={[styles.loadingContainer, styles.centerContainer]}> 
        			<ActivityIndicator size="large" color="#0000ff" />
        			<Text>Cargando Preguntas...</Text>
        		</View>
        	);
    	}else{
    		console.disableYellowBox = true;
			return(
				    <ScrollView>
        				{ this._renderAccordians() }
      			</ScrollView>
			);
		}
	}
	 _renderAccordians = () => {
	     const items = [];
	     const is_question = true;
	     for (var item of this.state.menu) 
	     {
	          items.push(<Accordian key={item.id} title = {item.title} data = {item.data} navigation = {this.props.navigation} question = {is_question} previous_answers = {this.state.previous_answers} all_questions = {this.state.data} id_element = {this.state.id_element}/>);
	     }
	     return items;
     }

}
