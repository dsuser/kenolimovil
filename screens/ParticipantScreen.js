import React, { Component } from 'react';
import { Text, ActivityIndicator, View, AsyncStorage, Button, ScrollView, TouchableOpacity, TouchableHighlight, Dimensions} from 'react-native';
import { Card, ListItem } from 'react-native-elements'
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import SearchInput, { createFilter } from 'react-native-search-filter';
import Icon from "react-native-vector-icons/MaterialIcons";
import NetInfo from "@react-native-community/netinfo";
import * as SQLite from 'expo-sqlite';
import { NavigationActions, StackActions } from 'react-navigation';
const db = SQLite.openDatabase('answersData.db');

import styles from '../styles/styles';
import urlBase from '../enviroment/url';
const win = Dimensions.get('window');
const KEYS_TO_FILTERS = ['name_participant'];
const now = new Date();
const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'Participants' })],
});
const no_participant = "Aun no hay participantes disponibles para este monitoreo";
export default class HomeScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      url: urlBase+'admin/movil/participants/json',
      answers: '',
      participants: '',
      searchTerm: '',
    }
  }
  static navigationOptions = {
    title: 'Seleccione Participante',
  };

  searchUpdated(term) {
    this.setState({ searchTerm: term })
  }

  componentDidMount(){
     this._retrieveData();
  }

    

    _checkConnectivity = async (answers) => {
     let isOffline = await AsyncStorage.getItem('isOffline');
      NetInfo.fetch().then(state => {
        if(isOffline  == "TRUE"){
          console.log("No Conectado");
         this._getStoredParticipants(answers);
        }else{
          if(state.isConnected){
            console.log("Conectado");
            this._getData(answers);
          }else{
            console.log("No Conectado");
            this._getStoredParticipants(answers);
          }
        }
      });
    }

    _restartScreen = () => {
    RNRestart.Restart();
   }

    _getStoredParticipants =(answers) => {
      this._restartScreen
       let community = answers.com;
       var form_id = this.props.navigation.getParam('form_id');
       db.transaction(tx => {
       tx.executeSql('SELECT participants_array FROM participants_table WHERE form_id = ? AND community = ?', [form_id, community], 
       (tx, results) => {
        console.log(results);
         if(results.rows._array[0]!=undefined){
          let participants = JSON.parse(results.rows._array[0]["participants_array"]);
          if(participants.data[0]!=undefined){
             // console.log(participants.data);
             this.setState({participants: participants.data})
          }
          else{
            let participants = {data: [{'id': 'xxxxxxxxxxx', 'name_participant': no_participant}]};
            this.setState({participants: participants.data})
          }
         
         }else{
            let participants = {data: [{'id': 'xxxxxxxxxxx', 'name_participant': no_participant}]};
            this.setState({participants: participants.data})
         }
       });
      });

    }

  _getData(answers){
        if(answers!=undefined){
        let community = answers.com;
        let form_id = this.props.navigation.getParam('form_id');
        let form_code = this.props.navigation.getParam('code');
        var formData = new FormData();
        formData.append("community", community);
        formData.append("form_id", form_id);
        formData.append("form_code", form_code);
        fetch(this.state.url, {
          method: 'POST',
          headers: { 'Accept': 'application/json','Content-Type': 'multipart/form-data',}, 
          body: formData,
        })
          .then(response => response.json())
          .then(participants => { 
            console.log(participants.data.length);
            if(participants.data.length > 0){
              var vacio = false;
              db.transaction(txn => {
                txn.executeSql("SELECT * from participants_table WHERE form_id = ? AND community = ?",[form_id, community],
                  function(tx, res) {
                    if(res.rows.length == 0){
                      vacio = true;
                      if(vacio){
                        txn.executeSql("INSERT INTO participants_table (participants_array, form_id, community) VALUES (?,?)",[JSON.stringify(participants),form_id,community],     
                          (tx, res) => {
                            console.log('Results', res.rowsAffected);
                            if (res.rowsAffected > 0) {
                              console.log("Participantes Guardados")
                            }else {
                              console.log("Algo Salio Mal");
                            }
                          });
                      }
                    }
                    else{
                      console.log("Se actualizara")
                      txn.executeSql("UPDATE participants_table SET participants_array = ? WHERE form_id = ? AND community = ?",[JSON.stringify(participants),form_id, community],     
                        (tx, res) => {
                          console.log('Results', res.rowsAffected);
                          if (res.rowsAffected > 0) {
                            console.log("Participantes Actualizados")
                          } else {
                            console.log("Algo Salio Mal");
                          }
                        });
                    }
                  }   
                  );
              });
              this.setState({participants: participants.data})

            }else{
              let participants = {data: [{'id': 'xxxxxxxxxxx', 'name_participant': no_participant}]};
              this.setState({participants: participants.data})
            }                    
          });
      }
     
  }

  _retrieveData = async () => {
  try {
    const answers = await AsyncStorage.getItem('answers');
      if (answers != null) {
       this.setState({answers: JSON.parse(answers)}, () => this._checkConnectivity(this.state.answers));
      }
    } catch (error) {
      console.log(error);
    }
  };


  render() {

    if(this.state.participants == ''){
      return(
                <View style={[styles.loadingContainer, styles.centerContainer]}> 
                  <ActivityIndicator size="large" color="#0000ff" />
                  <Text>Cargando Participantes...</Text>
                </View>
            );
    }
    else{
      const form_id = this.props.navigation.getParam('form_id');
      const form_name = this.props.navigation.getParam('form_name');
      const code = this.props.navigation.getParam('code');
      const filteredParticipants = this.state.participants.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS))

      return (
        <ScrollView>
         
          <View style={{ padding: 10, paddingBottom: 0 }}>
              <Text style={{ fontSize: 13, color: "rgba(0, 0, 0, 0.5)" }} >
                  {now.toDateString()}
                </Text>
          </View>
          <View>
          <Card containerStyle={[{ borderRadius: 20, elevation:0, height: win.height - 150 }]} title="Seleccione Participante" >
           <SearchInput 
              onChangeText={(term) => { this.searchUpdated(term) }} 
              style={styles.searchInput}
              placeholder="Escriba para buscar"
          />
          <React.Fragment >
          <ScrollView style={styles.scrollView}>
          {filteredParticipants.map(participante => {
            if(participante.name_participant.trim() == no_participant){
              return (
                <TouchableOpacity key={participante.id} style={styles.filterItem}>
                  <View>
                    <Text><Icon name={'warning'} size={16} color={'rgb(153, 204,255)'}/>{"  "}{participante.name_participant}</Text>
                  </View>
                </TouchableOpacity>
              )
            }
            else{
              return (
                <TouchableOpacity onPress={()=>this._showQuestions(form_id, form_name, code, participante.id, participante.name_participant)} key={participante.id} style={styles.filterItem}>
                  <View>
                    <Text><Icon name={'account-circle'} size={16} color={'rgb(153, 204,255)'}/>{"  "}{participante.name_participant}</Text>
                  </View>
                </TouchableOpacity>
              )
            }
            
          })}
        </ScrollView>
        </React.Fragment>
         </Card>
          </View>
        
        </ScrollView>
        
      );
    }
  }

   _showQuestions = (form_id, name, form_code, participant_id, participant_name) => {

      this.props.navigation.navigate('Question',{form_id: form_id, form_name: name, code: form_code, participant_id: participant_id, participant_name: participant_name});
  };

}
