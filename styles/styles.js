import { StyleSheet, Dimensions } from 'react-native';
import { Colors } from '../enviroment/Colors';
const win = Dimensions.get('window');
const styles = StyleSheet.create({
	/* ##### ESTILOS PARA LOGIN ##### */
	//contenedor principal
	mainContainer: {
    	alignItems: 'center',
     	paddingTop: 20,
     	backgroundColor: 'rgba(0,0,0,0.4)',
     	paddingBottom :10,
      width: 400,
      height: 300
  	},
	//Contenedor
	container : {
      flex: 5,
    	alignItems: 'center',
     	paddingTop: win.height - 370,
  	},
    questionContainer : {
      paddingTop: 0,
      paddingBottom: 70
      // flex: 1,
      // alignItems: 'center',
      // paddingTop: win.height - 370,
    },
	//Imagenes de fondo
	imageBackground:{
  		flex: 1,
    	resizeMode: 'cover'
	},
	//Contenedor de textos
	textContainer: {
    	flexDirection: 'row',
    	justifyContent: 'flex-start',
    	paddingLeft :25,
    	paddingBottom :0,
    	width: 350,
      marginTop :5
 	},
  textExit:{
    textAlign:'center' 
  },
 	//Texo del login
 	loginText: {

    	fontSize:26,
    	fontWeight:'100',
    	color:'#ffffff',
    	textAlign:'right',
      marginTop :5
 	},
  inconLogin:{
    padding: 8
  },
  actionsButtons:{
    flexDirection: 'column',
  },
  controlButtons:{
    flexDirection: 'row',
  },
 	//Contenedor de entradas de datos
 	inputContainer: {
		borderBottomColor: '#F5FCFF',
		backgroundColor: '#FFFFFF',
		borderRadius:15,
		borderBottomWidth: 1,
		width:300,
		height:45,
		marginBottom:0,
		flexDirection: 'row',
		// justifyContent: 'flex-end',
    marginTop :10
  	},
  	//Texto de los inputs
  	inputText:{
		height:45,
		marginLeft:30,
		borderBottomColor: '#FFFFFF',
		flex:1,
  	},
  	containerButtons: {
		flexDirection: 'row',
		justifyContent: 'flex-end',
		paddingTop: 10,
		paddingRight :20,
		paddingBottom :10,
		width: 350,
    },
    textAreaContainer: {
    borderRadius: 5,
    borderColor: 'grey',
    borderWidth: 0.5,
    padding: 5
  },
  textArea: {
    height: 150,
    justifyContent: "flex-start",
    textAlignVertical: 'top'
  },
  	//Botones
	button: {
		height:40,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		marginBottom:20,
		width:100,
		borderRadius:10,
    width:'100%',
    height:54,
    alignItems:'center',
    paddingLeft:35,
    paddingRight:35,
    },
    loginButton: {
    	backgroundColor: 'rgb(153, 204,255)',
  	},
  	buttonText: {
    	color: 'white',
    	fontSize: 18,
  	},
    containerButtonsLogin: {
    // flexDirection: 'column',
    //justifyContent: 'flex-end',
    marginLeft: 90,
    paddingTop: 20,
    paddingRight :0,
    paddingBottom :0,
    width: 200,
    },
    buttonTextLogin: {
      color: 'white',
      fontSize: 24,
    },
    buttonLogin: {
    // height:40,
    // flexDirection: 'row',
    justifyContent: 'center',
    // alignItems: 'center',
    marginBottom:20,
    borderRadius:10,
    width:'100%',
    height:50,
    alignItems:'center',
    paddingLeft:35,
    paddingRight:35,
    },
 	/* ##### FIN ESTILOS PARA LOGIN ##### */

 	/* ##### ESTILOS PARA CARD ##### */
 	footerCard:{
 		width: "100%",
        borderRadius: 4,
        padding: 2,
        backgroundColor: "rgba(255, 255, 255, 0.7)",
        position: "relative",
        bottom: 0,
        left: 0
 	},
 	textCard:{
 		fontSize: 18,
        marginBottom: 9,
        fontWeight: "bold",
        color: "rgba(0, 0, 0, 0.7)"
 	},
 	/* ##### ESTILOS PARA CARD  ##### */

  /* ##### ESTILOS PARA ACORDION ##### */
   accordian: {
   flex:1,
   paddingTop:100,
   backgroundColor:'#1abc9c',
   
  },
  /* ##### FIN ESTILOS PARA ACORDION ##### */

  /* ##### ESTILOS PARA TARJETAS DE PREGUNTAS ##### */
  containerNext: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    paddingTop: 15,
    paddingRight :0,
    paddingBottom :10,
    width: '95%',
    },
  buttonNext: {
    position: 'relative',
    height:50,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:10,
    marginLeft: 25,
    width:110,
    borderRadius:5,
  },
  buttonColor: {
      backgroundColor: 'rgb(48, 148,254)',
      marginLeft: 100
  },

  buttonDisabled: {
    position: 'relative',
    height:40,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:10,
    width:250,
    borderRadius:10,
    marginTop:10,
    backgroundColor: 'gray',
  },
  buttonEnabled: {
    position: 'relative',
    height:40,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:10,
    width:250,
    borderRadius:10,
    marginTop:10,
    backgroundColor: 'green',
  },
  contentdetalle:{
    flex: 1, 
    paddingTop :50,
    padding: 30,
    marginBottom : 30
  },

  /* ##### ESTILOS PARA TARJETAS DE PREGUNTAS ##### */

  /* ##### ESTILOS PARA CONTENEDORES DE CARGA ##### */
  loadingContainer:{
     flex: 1,
     justifyContent: 'center'

  },
  centerContainer:{
    flex: 1, 
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute', 
    top: 0, 
    left: 0, 
    right: 0, 
    bottom: 0,  
  },
  scrollView:{
    marginTop: 0, 
    marginBottom: 90,
  },
  /* ##### FIN ESTILOS PARA CONTENEDORES DE CARGA ##### */
  /* ##### ESTILOS PARA CHECKBOXES ##### */
  checkboxesContainer:{
    margin:2, 
    borderRadius: 8, 
    borderColor: "rgba(255, 255, 255, 0.7)",  
    elevation:0, 
    paddingTop: 0, 
    backgroundColor: "rgba(255, 255, 255, 0.7)"
   },
   checkboxesText:{
    fontSize: 12, fontWeight: 'normal',
   },
  /* ##### FIN ESTILOS PARA CHECKBOXES ##### */
/* ##### ESTILOS PARA BUSQUEDA ##### */
  searchInput:{
    padding: 4,
    borderColor: '#CCC',
    borderWidth: 1,
    elevation: 0,
    borderRadius: 5
  },
  filterItem:{
    borderBottomWidth: 0.5,
    borderColor: 'rgba(0,0,0,0.2)',
    padding: 10
  },
  /* ##### FIN ESTILOS PARA BUSQUEDA ##### */

  /* ##### ESTILOS PARA ELEMENTOS GUARDADOS ##### */
  sendButtons:{
    position: 'relative',
    height:40,
    borderRadius:5,
    marginTop:10,
    backgroundColor: 'rgba(38,114,38,0.7)',
  },
  savedButtons:{
    position: 'relative',
    height:40,
    borderRadius:5,
    marginTop:10,
    backgroundColor: 'rgba(48, 148,254,0.7)',
    marginLeft: 100
  },
  editButtons:{
    position: 'relative',
    height:40,
    borderRadius:5,
    marginTop:10,
    backgroundColor: 'rgba(227,165,73,0.7)',
  },
  deleteButtons:{
    position: 'relative',
    height:40,
    marginTop:10,
    borderRadius:5,
    backgroundColor: 'rgba(233, 47,47,0.7)',
  },

  saveButtons:{
    position: 'relative',
    height:40,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:10,
    marginTop:10,
    width:150,
    borderRadius:10,
    marginLeft: 10,
    backgroundColor: 'lightblue',
  },
  boldText:{
      fontSize:17,
      fontWeight:'bold',
      color:'black',
      textAlign:'left',
      marginTop :5
  },
  normalText:{
    fontSize:17,
    fontWeight:'100',
    color:'black',
    textAlign:'left',
    marginTop :5,
    width: '100%'
  },
  itemList:{ 
    backgroundColor: 'white', 
    padding: 20, 
    borderRadius: 20,
    flex: 1,
    flexWrap: 'wrap',
    flexDirection: 'row',
    paddingTop: 20, 
  },

  infoLeft:{
    flex: 1,
    flexDirection: 'column',
  },
  infoRight:{
    flex: 1,
    flexDirection: 'column'
  },

  /* ##### FIN ESTILOS PARA ELEMENTOS GUARDADOS ##### */

  
     buttonQ:{
        width:'100%',
        height:75,
        alignItems:'center',
        paddingLeft:20,
        paddingRight:35,
        //fontSize: 14,
    },
    title:{
        fontSize: 18,
        fontWeight:'bold',
        color: Colors.DARKGRAY,
    },
    itemActive:{
        fontSize: 12,
        color: Colors.GREEN,
    },
    itemInActive:{
        fontSize: 12,
        color: Colors.DARKGRAY,
    },
    btnActive:{
        borderColor: Colors.GREEN,
    },
    btnInActive:{
        borderColor: Colors.DARKGRAY,
    },
    row:{
        elevation:2,
        padding: 12,
        borderRadius: 5,
        flexDirection: 'row',
        justifyContent:'space-between',
        height:50,
        paddingLeft:25,
        paddingRight:30,
        alignItems:'center',
        backgroundColor: 'rgba(255,255,255,1.0)',
    },
    childRow:{
        flexDirection: 'row',
        justifyContent:'space-between',
        backgroundColor: Colors.GRAY,
    },
    childRowQ:{
        flexDirection: 'row',
        // justifyContent:'space-between',
        backgroundColor: Colors.GRAY,
    },
    parentHr:{
        height:1,
        color: Colors.WHITE,
        width:'100%'
    },
    childHr:{
        height:1,
        backgroundColor: Colors.LIGHTGRAY,
        width:'100%',
    },
    colorActive:{
        borderColor: Colors.GREEN,
    },
    colorInActive:{
        borderColor: Colors.DARKGRAY,
    },
    backgroundButton:{
      marginTop: 5,
      borderRadius: 20, 
      elevation: 1,
      width:'100%',
      backgroundColor: 'white',
    },
    buttonSave: {
    position: 'relative',
    height:50,
    flexDirection: 'row',
    // justifyContent: 'right',
    alignItems: 'center',
    marginTop:10,
    marginBottom:10,
    marginLeft: 25,
    width:110,
    borderRadius:5,
  },
   

});

export default styles;