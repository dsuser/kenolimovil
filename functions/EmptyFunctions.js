import {Alert} from 'react-native';
class EmptyFunctions {

	validarFormatoFecha(campo) {
      var RegExPattern = /^\d{2}[./-]\d{2}[./-]\d{4}$/;
      if ((campo.match(RegExPattern)) && (campo!='')) {
            return true;
      } else {
            return false;
      }
	}

	numericNoEmpty = (resultado) => {
      if(resultado != ""){ return true }
      else{ Alert.alert('Atencion!!','Por favor ingrese una cantidad'); return false; }
	};

	dateNoEmpty = (resultado) => {
		if(resultado != ""){ 
			let reverse_date = resultado.split("-").reverse().join("-");
	        let fecha = new Date(reverse_date);
	        if(this.validarFormatoFecha){
	        	if(!isNaN(fecha)){
	       			return true;
		        }else{
		            Alert.alert('Atencion!!','La fecha ingresada es invalida');
		            return false;
		        }
	        }else{
	        		Alert.alert('Atencion!!','El formato de fecha ingresado no es valido');
		            return false;
	        }
		}
        else{ Alert.alert('Atencion!!','Por favor ingrese una fecha'); return false; }
	};

	textNoEmpty = (resultado) => {
		if(resultado != ""){ return true }
        else{ Alert.alert('Atencion!!','Por favor ingrese datos validos'); return false; }
	};

	optionNoEmpty = (resultado) => {
		if(resultado != ""){ return true }
        else{ Alert.alert('Atencion!!','Debe seleccionar al menos una opcion'); return false; }
	};

}
const Empty = new EmptyFunctions();
export default Empty;