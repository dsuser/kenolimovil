class CalculationFunctions {
	//Funcion para el calculo de Edad
	calculateAge = (fecha_ingresada, fecha_actual) => {
	  let reverse_date = fecha_ingresada.split("-").reverse().join("-");
      fecha_ingresada = new Date(reverse_date);
      var edad = fecha_actual.getFullYear() - fecha_ingresada.getFullYear();
      var m = fecha_actual.getMonth() - fecha_ingresada.getMonth();
      if (m < 0 || (m === 0 && fecha_actual.getDate() < fecha_ingresada.getDate())) {
          edad--;
      }
     return edad;
    }
}
const Calculation = new CalculationFunctions();
export default Calculation;