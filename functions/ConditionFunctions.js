import {Alert} from 'react-native';
class ConditionFunctions {

	// validarFormatoFecha(campo) {
 //      var RegExPattern = /^\d{2}[./-]\d{2}[./-]\d{4}$/;
 //      if ((campo.match(RegExPattern)) && (campo!='')) {
 //            return true;
 //      } else {
 //            return false;
 //      }
	// }

	questionCondition = (question) => {
     return ((question['result_min'] == -1 && question['result_max'] == -1) || (question['result_min'] == null && question['result_max'] == null)) ? false : true;
	};

	dateCondition = (resultado,question) => {
		let reverse_date = resultado.split("-").reverse().join("-");
        let fecha = new Date(reverse_date);
        // if(this.validarFormatoFecha){
        	// if(!isNaN(fecha)){
        	let anio = fecha.getFullYear();
       		if(anio >= question['result_min'] && anio <= question['result_max']){ //si el ano esta en el rango devuelve true
              return true;
        	}else{
              Alert.alert('Atencion!!','Por favor ingrese fechas comprendidas desde el Año ' + question['result_min'] + ' hasta ' + question['result_max'] + '');
              return false;
        	}
	        // }
	        // else{
	            // Alert.alert('Atencion!!','La fecha ingresada es invalida');
	            // return false;
	        // }
        }
        // else{
        // 		Alert.alert('Atencion!!','El formato de fecha ingresado no es valido');
	       //      return false;
        // }
    

	integerCondition = (resultado,question) => {
		if(resultado >= parseInt(question['result_min']) && resultado <= parseInt(question['result_max'])){ //si el resultado esta en el rango devuleve true
			return true;
		}
		else{ 
			Alert.alert('Atencion!!','Por favor ingrese valores entre '+ question['result_min'] +' Y ' + question['result_max']+ ' '); 
			return false;
		}
	}

	decimalCondition = (resultado,question) => {
		 if(resultado >= parseFloat(question['result_min']) && resultado <= parseFloat(question['result_max'])){ 
		 	return true;
		 }
         else{ 
         	Alert.alert('Atencion!!','Por favor ingrese valores entre '+ question['result_min'] +' Y ' + question['result_max']+ ' '); 
         	return false;
         }
	}

	simpleCondition = (resultado,question) => {
		if(resultado >= question['result_min'] && resultado <= question['result_max']){ 
			return true;
		}
		else{ 
			Alert.alert('Atencion!!','Por favor seleccione una opcion'); 
			return false; 
		}       
	}

	multipleCondition = (resultado,question) => {
		if((Object.keys(resultado)).length >= question['result_min'] && (Object.keys(resultado)).length <= question['result_max']){ 
			return true;
		}
		else{ 
			Alert.alert('Atencion!!','Debe seleccionar entre ' + question['result_min'] + ' Y ' + question['result_max'] + 'Opciones'); 
			return false; 
		}       
	}

	jumpCondition = (resultado, question) => {
		return (resultado >= question['next_min'] && resultado <= question['next_max']) ? true : false;
	}

}
const Condition = new ConditionFunctions();
export default Condition;