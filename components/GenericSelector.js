 import React, {Component} from 'react';
import {ScrollView, Text} from 'react-native';

  //Elementos Genericos
import TextControl    from './TextControl';
import NumericControl from './NumericControl';
import DecimalControl from './DecimalControl';
import SelectControl  from './SelectControl';
import RadioControl   from './RadioControl';
import CheckControl   from './CheckboxControl';
import DateControl    from './DateControl';
import WelcomeControl from './WelcomeControl';

export default class GenericSelector extends Component {
  constructor(props) {
        super(props);
    }

  render() {
        if(this.props.question == undefined){
          return (<Text> No se han encontrado elementos disponibles </Text>);
        }
        else
        {
          switch(this.props.question['type_result'])
          {
            case "0": 
            return  <TextControl    label={this.props.question['title_question']} get_value={this.props._getValue} resultado={this.props.resultado}/> 
            break;
            case "1": 
            return  <DateControl    label={this.props.question['title_question']} get_value={this.props._getValue} resultado={this.props.resultado}/> 
            break;
            case "2": 
            return  <NumericControl label={this.props.question['title_question']} get_value={this.props._getValue} resultado={this.props.resultado}/> 
            break;
            case "3": 
            return  <DecimalControl label={this.props.question['title_question']} get_value={this.props._getValue} resultado={this.props.resultado}/> 
            break;
            case "4": 
            return  <RadioControl   label={this.props.question['title_question']} get_value={this.props._getValue} radio_props={this.props.question['result_question']} resultado={this.props.resultado}/> 
            break;
            case "5": 
            return  <ScrollView>
                      <CheckControl label={this.props.question['title_question']} get_value={this.props._getValue} checkboxes={this.props.question['result_question']} resultado={this.props.resultado}/>
                    </ScrollView> 
            break;
            case "9": 
            return  <WelcomeControl label={this.props.question['title_question']} /> 
            break;
          }
        }
}}