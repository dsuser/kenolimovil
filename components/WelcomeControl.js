import React from 'react';
import { Text, TextInput, View} from 'react-native';
import styles from '../styles/styles';
import { Card } from 'react-native-elements'

export default class WelcomeControl extends React.Component{
  constructor(props) {
    super(props);
    this.state = {};
    
  }
  _handlePress(text)
  {
    this.props.get_value(text);
  }
  render() {
    let array_text = this.props.label.split("<br/> ");
    let welcome_text = array_text[0];
    let title_text = array_text[1];
    let description_text = array_text[2];
    let thanks_text = array_text[3];
    if(title_text != undefined && description_text != undefined && thanks_text != undefined){
      return (  
        <View>
              
              <Card containerStyle={[{ borderRadius: 20, elevation:0, height: 350}]}  >
                <View>
                  <Text style={[{textAlign: 'center', fontSize: 25, color: "#4CAF50"}]}>{welcome_text}{"\n"}</Text>
                  <Text style={[{textAlign: 'justify', fontSize: 14}]}>{title_text}{"\n"}</Text>
                  <Text style={[{textAlign: 'justify', fontSize: 14}]}>{description_text}{"\n"}</Text>
                  <Text style={[{textAlign: 'center', fontSize: 18}]}>{"i"}{thanks_text}{"!"}</Text>
                </View>
              </Card>
        </View>
      );
    }else{
      return (  
        <View>
              <Card containerStyle={[{ borderRadius: 20, elevation:0, height: 300}]} title=' ' image={require('../assets/images/registros.jpg')} >
                <View style={styles.footerCard}>
                  <Text style={styles.textCard}>{welcome_text}{"\n"}</Text>
                </View>
              </Card>
        </View>
      );
    }
  }

}

