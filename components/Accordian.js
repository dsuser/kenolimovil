import React, {Component} from 'react';
import {Alert} from 'react-native';
import { View, TouchableOpacity, Text, FlatList, StyleSheet, TouchableHighlight, YellowBox} from "react-native";
import { Colors } from '../enviroment/Colors';
import Icon from "react-native-vector-icons/MaterialIcons";
import styles from '../styles/styles';
import Modal from 'react-native-modal';
import * as SQLite from 'expo-sqlite';
const db = SQLite.openDatabase('answersData.db');
import { NavigationActions, StackActions } from 'react-navigation';



//Elementos Genericos
import GenericSelector    from '../components/GenericSelector';
import Calculation        from '../functions/CalculationFuntions';
import Empty              from '../functions/EmptyFunctions';
import Condition          from '../functions/ConditionFunctions';

var resultado = "";
var order = undefined;
var input = undefined;
var current_question = undefined;
var no_jump = false;
var siguiente_indicador = undefined;
export default class Accordian extends Component{

    constructor(props) {
        super(props);
        this.state = { 
          data: props.data,
          all_questions: props.all_questions,
          expanded : false,
          modalVisible: false,
          previous_answers: props.previous_answers,
          updated: undefined
        }
    }

    componentDidMount(){
         resultado = "";
         order = undefined;
         input = undefined;
         current_question = undefined;
         no_jump = false;
         siguiente_indicador = undefined;
         // console.log(this.state.previous_answers.form_id)
    }

    _setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

     _validate  = (question, resultado) => {
      switch (question['type_result']) {
        case "0": return Empty.textNoEmpty(resultado);    break;
        case "1": return Empty.dateNoEmpty(resultado);    break;
        case "2": return Empty.numericNoEmpty(resultado); break;
        case "3": return Empty.numericNoEmpty(resultado); break;
        case "4": return Empty.optionNoEmpty(resultado);  break;
        case "5": return Empty.optionNoEmpty(resultado);  break;
        case "9": return true;                            break;
        default : console.log("Vacio Default");           break;
      }
    }

    _condicionated = (question) => {
      if(Condition.questionCondition(question)){
        switch (question['type_result']) {
          case "1": return Condition.dateCondition(resultado,question);     break;
          case "2": return Condition.integerCondition(resultado,question);  break;
          case "3": return Condition.decimalCondition(resultado,question);  break;
          case "4": return Condition.simpleCondition(resultado,question);   break;
          case "5": return Condition.multipleCondition(resultado,question); break;
          case "9": return false;                                           break;
          default : console.log("Condicion Default");                       break;
        }
      }else{
        return Condition.questionCondition(question);
      }
    }
    _updateDatabaseNoJump = (orden,previous) => {
        previous[orden] = resultado
        this._updateDatabase(previous);
        this._setModalVisible(!this.state.modalVisible);
        this.props.navigation.navigate('ListQuestion',{id: this.props.id_element, answers: previous});
       
    }

    _updateDatabase = (previous) => {
        console.log("SE GUARDARA");
        console.log(previous);
        console.log("ELEMENTO: " + this.props.id_element);
        console.log("Se actualizara");
        db.transaction(txn => {
            txn.executeSql(
                "UPDATE answer_table SET answer_array = ? WHERE id = ?",
                [JSON.stringify(previous),this.props.id_element],     
                (tx, res) => {
                    console.log('Results', res.rowsAffected);
                    if (res.rowsAffected > 0){
                        console.log(res)
                        console.log("Respuestas Actualizadas")
                        } 
                    else{
                        console.log("Algo Salio Mal");
                    }
            });
        });
        db.transaction(txn => {
            txn.executeSql(
                "SELECT * FROM answer_table WHERE id = ?",
                [this.props.id_element],     
                (tx, res) => {
                    console.log('Results', res);
            });
        });
        this.setState({previous_answers : previous});
        this.props.navigation.navigate('ListQuestion',{id: this.props.id_element, answers: previous});


    }

    _Jump = (orden) => {
        let previous_answers = this.state.previous_answers;
        previous_answers[orden] = resultado
        this._updateDatabase(previous_answers);
        this._setModalVisible(!this.state.modalVisible);
    }

    _noJump = (orden) => {
        //Si el llenado es secuencial se debe hacer lo siguiente
        /*Obtener la pregunta en su indice respectivo
        * Obtener el orden de esa pregunta
        */
        let pregunta = this.state.all_questions[orden];
        // console.log("ORDEN ANTES DE PREGUNTA: " +pregunta.orden);
        // console.log("RESULTADO ANTERIOR: " +resultado);
        let previous_answers = this.state.previous_answers;
        previous_answers[pregunta.orden] = resultado;
        this._updateDatabase(previous_answers);
        
    }

    
    /*Para editar una pregunta guardada se necesita lo siguiente
    * 1- El orden de la pregunta 
    * 2- Las respuestas anteriores
    * 3- Todas las preguntas para validar
    */
    _editQuestion = (orden, previous_answers) => {
        if(no_jump){
            // console.log("ORDEN QUE VIENE: " + orden);
            // console.log("RESULTADO ANTERIOR: " +resultado);
            /*Si son preguntas con salto se hace lo siguiente
            * Para estos casos no es el orden de lo guardado sino el indice de la pregunta
            * Se obtiene la pregunta que representa ese indice
            */
            if(orden < siguiente_indicador){
                let pregunta_anterior = this.state.all_questions[orden-1];
                if(this._validate(pregunta_anterior,resultado)){
                    let pregunta_nueva = this.state.all_questions[orden];
                    let resultado_vacio = "";
                    input = <GenericSelector question = {pregunta_nueva} _getValue = {this._getValue} resultado = {resultado_vacio} />;
                    order = orden;
                    this._setModalVisible(true);
                }
            }else{
                no_jump = false;
                this._setModalVisible(!this.state.modalVisible);
            }
            
        }
        else{
            if(previous_answers[orden] != undefined){ //Verificamos si el orden esta en las respuestas anteriores
                /* Si se encuentra se realiza lo siguiente
                * Se asigna como resultado el valor de las pregunta_anterior en su orden encontrado
                * Se obtiene la pregunta en ese orden y se valida
                */
                resultado = previous_answers[orden];
                let question = this.state.all_questions.find(data => data['orden'] == orden);
                if(this._validate(question, resultado)){ //Si todo esto es valido
                    /*
                    * Se asigna en un control el resultado de una seleccion componente
                    * Se muestra el modal
                    */
                    input = <GenericSelector question = {question} _getValue = {this._getValue} resultado = {resultado} />;
                    order = orden;
                    this._setModalVisible(true);
                }
                else
                {
                    console.log(orden);
                }
            }
        }
    }

    
    /* Para Actualizar los resultados se debe cumplir lo siguiente
    * Obtener la pregunta Actual
    * Validar si la pregunta es Condicionada o no
    */
    _updateResult = (orden) => {
        let current_question = this.state.all_questions.find(data => data['orden'] == orden);
        if(this._condicionated(current_question)){
            /* Si la pregunta es condicionada se realiza lo siguiente
            * Verificar si la pregunta tiene salto o no
            * Si se cumple un salto se eliminan todos los indices anteriores a esa pregunta y se guarda el resultado actual
            */
            console.log("CON CONDICION");
            if(Condition.jumpCondition(resultado,current_question)){
                let all = this.state.all_questions;
                siguiente_indicador = all.findIndex(data => data['id'] == current_question['next_question']);
                let orden_actual = all.findIndex(data => data['orden'] == orden);
                let orden_siguiente = orden_actual+1;
                let previous = this.state.previous_answers;
                // let pregunta_anterior = this.state.all_questions[orden_actual];
                // console.log(pregunta_anterior)
                while(orden_actual < siguiente_indicador){
                    orden_actual++;
                    let pregunta = all[orden_actual];
                    console.log(pregunta);
                    delete previous[pregunta.orden]
                    
                }
                this._updateDatabaseNoJump(orden, previous);
                // this.setState({previous_answers: previous})
                // this._Jump(orden);
            }
            else{ //Si no hay salto se realiza lo siguiente
                /* Se llenan las preguntas de forma secuancial segun su flujo haciendo lo siguiente
                * en base al orden 
                */
                console.log("LLENADO SECUENCIAL");
                no_jump = true;
                let all = this.state.all_questions;
                siguiente_indicador = all.findIndex(data => data['id'] == current_question['next_question']);
                let orden_actual = all.findIndex(data => data['orden'] == orden);
                let orden_siguiente = orden_actual+1;
                this._noJump(orden_actual);
                this._editQuestion(orden_siguiente, this.state.previous_answers);

            } 
        }  
        else{ //Si no es condicionada
            /*Si no hay codicion se realiza lo siguiente
            * Se verifica que la pregunta no venga de un salto
            */
            if(no_jump){
                console.log("NO SALTO SIN CONDICION");
                console.log("ORDEN QUE VIENE: " + orden);
                console.log("RESULTADO ANTERIOR: " +resultado);
                let pregunta_anterior = this.state.all_questions[orden];
                if(this._validate(pregunta_anterior,resultado)){
                    console.log("RESPUESTAS VALIDAS, ACCION A NUEVA PREGUNTA");
                    let orden_siguiente = orden + 1;
                    this._noJump(orden);
                    this._editQuestion(orden_siguiente, this.state.previous_answers);
                }
                /* Si es pregunta con salto se realizara lo siguiente */
            }
            else{
                /* No hay salto se procede a guardar los resultados*/
                this._Jump(orden);
            }
            
        } 

    }

    _editMember(member){
        console.log("Funciones futuras");
        //this.props.navigation.navigate('Members',{member: member, form_id: this.state.previous_answers.form_id});
    }

    _getValue(value){
      resultado = value;

    }
  
    render() {
        console.disableYellowBox = true;
        if(this.props.question){
            return(

                <View>
                <View style={{flex: 1}} >
                    <Modal
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        this._setModalVisible(!this.state.modalVisible)
                    }}>
                        <View style={{marginTop: 22}}>
                            <View>
                                {input}
                            </View>
                            <View style = {styles.backgroundButton}>
                                <View>
                                    <TouchableOpacity style={this.state.disabled ?  [styles.buttonDisabled] : [styles.buttonSave, styles.buttonColor]}  onPress={()=>this._updateResult(order)} disabled={this.state.disabled}>
                                        <Text style={styles.buttonText} ><Icon name={"save"} size={30} color={'rgb(255, 255,255)'}/> Guardar </Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </Modal>
                </View>
                    <View>
                    <TouchableOpacity key = {Math.random()} style={styles.row} onPress={()=>this.toggleExpand()}>
                    <Icon name={'description'} size={30} color={'rgb(153, 204,255)'} />
                    <Text style={[styles.title]}>{this.props.title}</Text>
                    <Icon name={this.state.expanded ? 'keyboard-arrow-up' : 'keyboard-arrow-down'} size={30} color={'rgb(153, 204,255)'} />
                    </TouchableOpacity>
                    <View style={styles.parentHr}/>
                    {
                        this.state.expanded &&
                        <View key = {Math.random()}>
                            <FlatList
                            key = {Math.random()}
                            data={this.state.data}
                            numColumns={1}
                            scrollEnabled={true}
                            renderItem={
                                ({item, index}) =>{
                                    if(item.title_question){
                                        return(
                                            <View key = {Math.random()}>                                                                   
                                                <TouchableOpacity style={[styles.childRowQ, styles.buttonQ]} onPress={()=>this._editQuestion(item.orden, this.props.previous_answers)}>
                                                <Icon name={'edit'} size={24} color={'rgba(0,102,204,0.5)'} />
                                                <Text>{"   "}</Text>
                                                <Text>{item.title_question}</Text>
                                                </TouchableOpacity>
                                                <View style={styles.childHr}/>
                                            </View>
                                        );
                                    }else{
                                        if(this.props.previous_answers.form == "C30"){
                                            return(
                                                <View key = {Math.random()}>
                                                    <TouchableOpacity  style={[styles.childRowQ, styles.buttonQ]} onPress={()=>this._editMember(item)}>
                                                    <Icon  name={'info'} size={24} color={'rgba(0,102,204,0.5)'} />
                                                    <Text >{"   "}</Text>
                                                    <Text >{item['41']}</Text>
                                                    <Text >{item.form_id}</Text>
                                                    </TouchableOpacity>
                                                    <View  style={styles.childHr}/>
                                                </View>
                                            );
                                        }
                                        else if (this.props.previous_answers.form == "C31") {
                                            if(this.props.previous_answers.persons.length != 0){
                                                return(
                                                    <View key = {Math.random()}>
                                                        <TouchableOpacity  style={[styles.childRowQ, styles.buttonQ]} onPress={()=>this._editMember(item)}>
                                                        <Icon  name={'info'} size={24} color={'rgba(0,102,204,0.5)'} />
                                                        <Text >{"   "}</Text>
                                                        <Text >{item['1.4']}</Text>
                                                        <Text >{item.form_id}</Text>
                                                        </TouchableOpacity>
                                                        <View  style={styles.childHr}/>
                                                    </View>
                                                );
                                            }
                                        }else if (this.props.previous_answers.form == "C32") {
                                            if(this.props.previous_answers.persons.length != 0){
                                                return(
                                                        <View key = {Math.random()}>
                                                            <TouchableOpacity  style={[styles.childRowQ, styles.buttonQ]} onPress={()=>this._editMember(item)}>
                                                            <Icon  name={'info'} size={24} color={'rgba(0,102,204,0.5)'} />
                                                            <Text >{"   "}</Text>
                                                            <Text >{item['1.2']}</Text>
                                                            <Text >{item.form_id}</Text>
                                                            </TouchableOpacity>
                                                            <View  style={styles.childHr}/>
                                                        </View>
                                                );
                                            }
                                        }
                                        else if((this.props.previous_answers.form == "C40")){
                                              if(this.props.previous_answers.persons.length != 0){
                                                return(
                                                        <View key = {Math.random()}>
                                                            <TouchableOpacity  style={[styles.childRowQ, styles.buttonQ]} onPress={()=>this._editMember(item)}>
                                                            <Icon  name={'info'} size={24} color={'rgba(0,102,204,0.5)'} />
                                                            <Text >{"   "}</Text>
                                                            <Text >{item['1.1']}</Text>
                                                            <Text >{item.form_id}</Text>
                                                            </TouchableOpacity>
                                                            <View  style={styles.childHr}/>
                                                        </View>
                                                );
                                            }
                                        }
                                        
                                    }
                                } 
                            }
                            keyExtractor={(item) => {Math.random()}}
                            />
                        </View>
                    }
                    </View>
                </View>
            );
        }else{
           return (
                <View>
                <TouchableOpacity key = {Math.random()} style={styles.row} onPress={()=>this.toggleExpand()}>
                <Icon name={'airplay'} size={30} color={'rgb(153, 204,255)'} />
                <Text style={[styles.title]}>{this.props.title}</Text>
                <Icon name={this.state.expanded ? 'keyboard-arrow-up' : 'keyboard-arrow-down'} size={30} color={'rgb(153, 204,255)'} />
                </TouchableOpacity>
                <View style={styles.parentHr}/>
                {
                    this.state.expanded &&
                    <View key = {Math.random()}>
                        <FlatList
                        key = {Math.random()}
                        data={this.state.data}
                        numColumns={1}
                        scrollEnabled={true}
                        renderItem={
                            ({item, index}) => 
                            <View key = {Math.random()}>
                                <TouchableOpacity style={[styles.childRowQ, styles.buttonQ]} onPress={()=>this._showQuestions(item.value, item.name, item.code)}>
                                <Icon name={'near-me'} size={24} color={'rgba(0,102,204,0.5)'} />
                                <Text>{"   "}</Text>
                                <Text>{item.name}</Text>
                                </TouchableOpacity>
                                <View style={styles.childHr}/>
                            </View>
                        }
                        keyExtractor={(item) => {Math.random()}}
                        />
                    </View>
                }
                </View>
            ); 
        }
        
    }

  _showQuestions = (form_id, name, form_code) => {
    if(form_id >= 4 && form_id <= 6 ){
        this.props.navigation.navigate('Participants',{form_id: form_id, form_name: name, code: form_code});
    }
    else{
        this.props.navigation.navigate('Question',{form_id: form_id, form_name: name, code: form_code});
    }
    
  }

  onClick=(index)=>{
    const temp = this.state.data.slice()
    temp[index].value = !temp[index].value
    this.setState({data: temp})
  }

  toggleExpand=()=>{
    this.setState({expanded : !this.state.expanded})
  }

}

