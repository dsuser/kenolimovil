import React, { Component } from 'react'
import DatePicker from 'react-native-datepicker'
import { TextInputMask } from 'react-native-masked-text'
import { Text, View, TextInput} from 'react-native';
import { Card, ListItem, Icon } from 'react-native-elements'
import styles from '../styles/styles';
export default class MyDatePicker extends Component {
  constructor(props){
    super(props)
    this.state = {dt: this.props.resultado};
  }
 _handlePress(date)
  {
    this.props.get_value(date);
  }
  render(){
       return (
        <View>
          <Card containerStyle={[{ borderRadius: 20, elevation:0, height: 250}]} title={this.props.label} >
                <View style={styles.textAreaContainer}>
                  <TextInputMask
                    type={'datetime'}
                    placeholder="En formato DD-MM-YYYY"
                    placeholderTextColor="grey"
                    options={{
                      format: 'DD-MM-YYYY'
                    }}
                    value={this.state.dt}
                    onChangeText={text => {
                      this.setState({ dt: text},()=>this._handlePress(text))
                    }}
                    ref={(ref) => this.datetimeField = ref}
                  />
                </View>
          </Card>
        </View>
        
      );
      
  }
}