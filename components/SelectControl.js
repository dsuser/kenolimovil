import React, {Fragment} from 'react';
import { View, SafeAreaView, Alert,Text} from 'react-native';
import styles from '../styles/styles';
import MultiSelect from 'react-native-multiple-select';

export default class SelectControl extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
       selectedItems: [],
    };
  }

  onSelectedItemsChange = selectedItems => {
    this.setState({ selectedItems });
    //Set Selected Items
  };
  render() {
    const { selectedItems } = this.state;
    return (
      <View style={styles.contentdetalle}>
      <View style ={styles.textpregunta}>
      <Text>{this.props.label}</Text>
      </View>
       <SafeAreaView style={{ flex: 1 }}>
        <View style={{ flex: 1, padding: 30 }}>
          <MultiSelect
            hideTags
            items={this.props.items}
            uniqueKey="id"
            ref={component => {
              this.multiSelect = component;
            }}
            single = "true"
            onSelectedItemsChange={this.onSelectedItemsChange}
            selectedItems={selectedItems}
            selectText="Seleccione"
            searchInputPlaceholderText="Search Items..."
            onChangeInput={text => console.log(text)}
            tagRemoveIconColor="#CCC"
            tagBorderColor="#CCC"
            tagTextColor="#CCC"
            selectedItemTextColor="#CCC"
            selectedItemIconColor="#CCC"
            itemTextColor="#000"
            displayKey="name"
            searchInputStyle={{ color: '#CCC' }}
            submitButtonColor="#48d22b"
            submitButtonText="Submit"
          />
        </View>
      </SafeAreaView>
      </View>
    );
  }

}
