import React from 'react';
import { TextInputMask } from 'react-native-masked-text'
import { Text, TextInput, View} from 'react-native';
import styles from '../styles/styles';
import { Card } from 'react-native-elements'

export default class DecimalControl extends React.Component{
  constructor(props) {
    super(props);
    this.state = {dt: this.props.resultado};
  }

  _handlePress(text)
  {
    const regex = new RegExp("^$|^[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)$");
    if (regex.test(text)) {
      this.setState({ dt: text }, () => this.props.get_value(text));
    }
  }
  render() {
    return (
      <View>
            <Card containerStyle={[{ borderRadius: 20, elevation:0, height: 250}]} title={this.props.label} >
              <View style={styles.textAreaContainer}>
                <TextInput
                  keyboardType="numeric"
                  underlineColorAndroid="transparent"
                  placeholder="Ingrese su informacion"
                  placeholderTextColor="grey"
                  value={this.state.dt}
                  onChangeText={(text) => {
                      this._handlePress(text)
                    }}
                />
              </View>
            </Card>
      </View>
    );
  }

}

