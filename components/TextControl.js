import React from 'react';
import { Text, TextInput, View} from 'react-native';
import styles from '../styles/styles';
import { Card } from 'react-native-elements'

export default class TextControl extends React.Component{
  constructor(props) {
    super(props);
    this.state = {dt: this.props.resultado}; 
  }

  _handlePress(text)
  {
    this.props.get_value(text);
  }
  render() {
      return (
        <View>
              <Card containerStyle={[{ borderRadius: 20, elevation:0, height: 250}]} title={this.props.label} >
                <View style={styles.textAreaContainer}>
                  <TextInput 
                    style={styles.textArea}
                    underlineColorAndroid="transparent"
                    placeholder="Ingrese su informacion"
                    placeholderTextColor="grey"
                    numberOfLines={10}
                    multiline={true}
                    value = {this.state.dt}
                    onChangeText={text => {
                      this.setState({ dt: text},()=>this._handlePress(text))
                    }}
                  />
                </View>
              </Card>
        </View>
      );
  }

}

