import React from 'react';
import { Text, View, SafeAreaView, Alert, Dimensions} from 'react-native';
import styles from '../styles/styles';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import { Card, ListItem, Icon } from 'react-native-elements';

const win = Dimensions.get('window');
export default class RadioControl extends React.Component{

  constructor(props) {
    super(props);
    this.state = {dt: this.props.resultado};
  }


  _handlePress(selected)
  {
    this.props.get_value(selected);
  }
  render() {
    let radio_props = [];
    let opciones = this.props.radio_props.split(";").map((opcion, index) => {
      let ops = {label: opcion, value: index+1}
      radio_props.push(ops);
    }); 

    return (
     <View>
        <Card containerStyle={[{ borderRadius: 20, elevation:0, height: win.height - 300}]} title={this.props.label} >
           <RadioForm
            radio_props={radio_props}
            initial={(this.state.dt != "") ? this.state.dt - 1 : null}
            borderWidth={4}
            buttonSize={13}
            onPress={(value) => {this._handlePress(value)}}
          />
         </Card>
      </View>
      
       
    );
  }

}
