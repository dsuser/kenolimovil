import React, {Fragment} from 'react';
import {View, SafeAreaView, Alert, Text, ScrollView, Dimensions} from 'react-native';
import styles from '../styles/styles';
import { Card, ListItem, Icon, CheckBox } from 'react-native-elements';
//var contador = 0;
export default class CheckControl extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
       selectedItems: [],
       checked: true,
       value: 0,
       group_option: [],
    };
  }

  componentDidMount(){
    this._getOptions();
  }

  _getOptions = async () => {
    let contador = 0;
    let indice = 0;
    let object = []; 
    if( this.props.checkboxes.indexOf('@@') != -1){
      let grupos = this.props.checkboxes.split('@@');
      for (let i = 0; i < grupos.length; i++) {
        if (i%2==0) {
          var options = [];
          grupos[i+1].split(';').map((opcion, index) => {
             let ops = {label: opcion, value: contador, checked:false}
             contador++;
             options.push(ops);
          });
          object.push({id: indice , title: grupos[i],  options:options});
          indice++;
        }
      }
      this.setState({group_option: object});
    }else{
      let opciones = this.props.checkboxes.split(";").map((opcion, index) => {
        let ops = {label: opcion, value: index, checked:false}
        object.push(ops);
      }); 
      this.setState({group_option: object});
    }
    object = [];
    
  }

  _handlePress()
  {
    this.props.get_value(Object.assign({}, this.state.selectedItems));
  }

  _verify = (value, name, indice, checked, group) => {
    if(group){
      let group = this.state.group_option;
      group[indice].options.find(data => data.value === value).checked = !checked; 
      let selectedItems = this.state.selectedItems;
      !checked ? selectedItems.push(value) : selectedItems.splice( selectedItems.indexOf(value), 1 );
      this.setState({group_option: group, selectedItems: selectedItems});
      this._handlePress();
    }
    else{
      let group = this.state.group_option;
      group.find(data => data.value === value).checked = !checked;
      let selectedItems = this.state.selectedItems;
      !checked ? selectedItems.push(value) : selectedItems.splice( selectedItems.indexOf(value), 1 );
      this.setState({group_option: group, selectedItems: selectedItems});
      this._handlePress();
    }
  }

  render() {
    const win = Dimensions.get('window');
    let object = this.state.group_option;
    
      if( this.props.checkboxes.indexOf('@@') != -1){
      return (
        <View >
        <ScrollView>
            <Card containerStyle={[{ borderRadius: 20, elevation:0, height: win.height - 175}]} title={this.props.label} >
            {
              <ScrollView style={styles.scrollView}>{
              object.map((checkboxes, index) => 
                <React.Fragment key={checkboxes.index}>
                  <Text style="{[{ffontWeight: 'bold' }]}">{checkboxes.title}</Text>
                    {checkboxes.options.map((option, index) => {
                      if(this.props.resultado == ""){
                        return(
                            <CheckBox
                            containerStyle = {styles.checkboxesContainer}
                            size = {24}
                            textStyle = {styles.checkboxesText}
                            key = {option.value}
                            title= {option.label}
                            checked={option.checked}
                            onPress={() => this._verify(option.value, option.label, checkboxes.id, option.checked, true )}
                            />);
                      }
                      else{
                        // console.log(this.props.resultado)
                        console.log(Object.values(this.props.resultado).find(data => data == 24));
                        // console.log(Object.values(this.props.resultado).find(data => this.props.resultado[data] == option.value));
                        return(
                            <CheckBox
                            containerStyle = {styles.checkboxesContainer}
                            size = {24}
                            textStyle = {styles.checkboxesText}
                            key = {option.value}
                            title= {option.label}
                            checked={(Object.values(this.props.resultado).find(data => data == option.value) != undefined) ? !option.checked : option.checked}
                            onPress={() => this._verify(option.value, option.label, checkboxes.id, ((Object.values(this.props.resultado).find(data => data == option.value) != undefined) ? !option.checked : option.checked), true )}
                            />);
                        }
                           
                      }
                    )
                  }
                </React.Fragment>
              )}
              </ScrollView>
            }
          </Card>
          </ScrollView>
        </View>
      );
    }
    else{
      let object = this.state.group_option;
       return (
        <View >
        <ScrollView>
             <Card containerStyle={[{ borderRadius: 20, elevation:0, height: win.height - 175 }]} title={this.props.label} >
             {
                <React.Fragment key={this.props.checkboxes.index}>
                  {object.map((option, index )=>{
                  if(this.props.resultado == ""){
                    return(
                      <CheckBox
                        containerStyle = {styles.checkboxesContainer}
                        size = {24}
                        textStyle = {styles.checkboxesText}
                        key = {option.value}
                        title= {option.label}
                        checked={option.checked}
                        onPress={() => this._verify(option.value, option.label, index, option.checked, false)}
                        />
                    );
                  }else{
                    return(
                      <CheckBox
                        containerStyle = {styles.checkboxesContainer}
                        size = {24}
                        textStyle = {styles.checkboxesText}
                        key = {option.value}
                        title= {option.label}
                        checked={(Object.values(this.props.resultado).find(data => data == option.value) != undefined) ? !option.checked : option.checked}
                        onPress={() => this._verify(option.value, option.label, index, ((Object.values(this.props.resultado).find(data => data == option.value) != undefined) ? !option.checked : option.checked), false)}
                        />
                    );
                  } 
                }
                        
                    )}
                </React.Fragment>
            }
          </Card>
           </ScrollView>
        </View>

      );
    }
  }
}
