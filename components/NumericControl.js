import React from 'react';
import { Text, TextInput, View} from 'react-native';
import styles from '../styles/styles';
import { Card } from 'react-native-elements'
import { TextInputMask } from 'react-native-masked-text'
export default class NumericControl extends React.Component{
  constructor(props) {
    super(props);
    this.state = {dt: this.props.resultado};
  }

  _handlePress(text)
  {
    this.props.get_value(text);
  }
  render() {
    return (
      <View>
          <Card containerStyle={[{ borderRadius: 20, elevation:0, height: 250}]} title={this.props.label} >
                <View style={styles.textAreaContainer}>
                  <TextInputMask
                    type={'only-numbers'}
                    placeholder="Ingrese su informacion"
                    placeholderTextColor="grey"
                    value={this.state.dt}
                    onChangeText={text => {
                      this.setState({ dt: text},()=>this._handlePress(text))
                    }}
                  />
                </View>
          </Card>
        </View>
    );
  }
}
