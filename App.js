import * as React from 'react';
import { StyleSheet, View } from 'react-native';

import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

//screens app
import HomeScreen from './screens/HomeScreen';
import QuestionnaireScreen from './screens/QuestionnaireScreen';
import QuestionScreen from './screens/QuestionScreen'
import RegisterScreen from './screens/RegisterScreen';
import OtherScreen from './screens/OtherScreen';
import CommunityScreen from './screens/CommunityScreen';
import ParticipantScreen from './screens/ParticipantScreen';
import QListScreen from './screens/QListScreen';
import MemberScreen from './screens/MemberScreen';

//screens auth
import LoginScreen from './screens/LoginScreen';
import LoadingScreen from './screens/LoadingScreen';

const AppStack = createStackNavigator(
  { 
    Home: HomeScreen, 
    Questionnaire: QuestionnaireScreen,
    Question: QuestionScreen,
    Register: RegisterScreen,
    Communities: CommunityScreen,
    Participants: ParticipantScreen,  
    Other: OtherScreen ,
    ListQuestion: QListScreen,
    Members: MemberScreen
  }
);

const AuthStack = createStackNavigator(
  { 
    Login: LoginScreen 
  }
);

export default createAppContainer(
  createSwitchNavigator(
    {
      AuthLoading: LoadingScreen,
      App: AppStack,
      Auth: AuthStack,
    },
    {
      initialRouteName: 'AuthLoading',
    }
  )
);
